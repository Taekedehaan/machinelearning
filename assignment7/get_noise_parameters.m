clear all
close all
clc

%% init
disp('=====INTIT====')
p = [0.4 0.5 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4]; % [ 0.5, 1.3]
r = [10000, 3000, 2000, 1500, 1000, 800, 600, 400, 200, 150, 100, 80, 50, 30, 10]; %[ 2000
nRep = 20;
dim = [200, 200, 1000, 400];
pathFig = [pwd, filesep, 'fig', filesep];

%% load data
pathLabData = [pwd, filesep, 'data', filesep, 'train.csv'];
pathUnlabData = [pwd, filesep, 'data', filesep, 'test.csv'];

dataLab = load_labeled_data(pathLabData);
dataUnlab = load_unlabeled_data(pathUnlabData);

%% run noise simulation
disp('=====START NOISE SIMULATION====')

for ip = 1:length(p)
    disp(['New p:', num2str(p(ip))]);
    for ir = 1:length(r)
        
        difference = nan(1,nRep);
        for irep = 1:nRep
            labeledDataNoise = add_noise(dataLab, p(ip), r(ir));
            
            % get std's
            stdLabeledDataNoise = std(labeledDataNoise.x,1);
            stdUnlabData = std(dataUnlab.x,1);
            
            % compute difference
            % difference(irep) = mean(mean(dist(labeledDataNoise.x, unlabData.x')));
            % difference(irep) = mean((kurtosis(dataUnlab.x) - kurtosis(labeledDataNoise.x)).^2);
            difference(irep) = sum((stdUnlabData - stdLabeledDataNoise).^2);
        end
        
        % store difference
        meanDifference(ip, ir) = mean(difference);
        stdDifference(ip, ir) = std(difference);
        
    end
end

%% plot with error bar
color = gen_colors(length(p));
lineSpec = repmat(["-", "--", ":", "-."],1,3);

figure('Position', dim)
hold on
for i = 1:length(p)
    errorbar(r, meanDifference(i,:), stdDifference(i,:), lineSpec(i), 'Linewidth', 2, 'Color', color(i,:));
end

xlabel('radius')
ylabel('difference measure')

set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')

ylim([16.5, 17.5])
% ylim([0.10, 0.13])
xlim([min(r), max(r)])

legend([repmat('p =',length(p), 1), num2str(p')], 'Location', 'eastoutside')

saveas(gcf, 'optimization_errorbar', 'epsc')
saveas(gcf, 'optimization_errorbar', 'fig')

%% plot
figure('Position', dim)
hold on

for i = 1:length(p)
    plot(r, meanDifference(i,:), lineSpec(i), 'Linewidth', 2, 'Color', color(i,:));
end

title('Optimization of noise parameters')
xlabel('p-ball radius')
ylabel('dissimilarity')

set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')

ylim([16.4, 17.3])
% ylim([0.10, 0.13])
xlim([min(r), max(r)])

legend([repmat('p =',length(p), 1), num2str(p')], 'Location', 'eastoutside')

saveas(gcf, [pathFig, 'optimization'], 'epsc')
saveas(gcf, [pathFig, 'optimization'], 'fig')

