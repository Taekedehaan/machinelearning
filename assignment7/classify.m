clear all
close all
clc

%% init
disp('===INITIALIZING====')

% path
path = pwd;
figPath = [path, filesep,'fig', filesep];

% noise
r = 100;
p = 1;

% crossvall
nFold = 10;

% generative model 
threshold = 10^(-6);

% data repetaition
pRep = 0.5;

%% load data train
pathLabData = [pwd, filesep, 'data', filesep, 'train.csv'];
pathUnlabData = [pwd, filesep, 'data', filesep, 'test.csv'];
pathRamon = [pwd, filesep, 'ramon', filesep, 'label.csv'];

dataLab = load_labeled_data(pathLabData);
dataUnlab = load_unlabeled_data(pathUnlabData);
yRamon = csvread(pathRamon);

% add noise
% dataLab = add_noise(dataLab, p, r);

% normalize
[dataLab, dataUnlab, meanX, stdX] = normalize_data_struct2(dataLab, dataUnlab);

% get some properties
[~, nFeat] = get_properties(dataLab);

iRandom1 = randperm(dataLab.n);
%%
errorRateTest = nan(1, nFold);
errorRateTrain = nan(1, nFold);
differenceMeRamon = nan(1, nFold);

for i = 1:nFold
    disp('==SELECT NEW DATA SET==');
    if nFold == 1
        % split labled data
        [dataLabTrain, dataLabTest] = split_data(dataLab, 0.2);
    else
        range = dataLab.n/nFold;
        if round(range) ~= range
           error(['invalid number of cross validation folds: ', num2str(nFold)]);
        end
        
        indexStart = (i - 1) * range + 1;
        indexEnd = i * range;
        
        indexTest = indexStart:indexEnd;
        indexTrain = 1:dataLab.n;
        indexTrain(indexTest) = [];
        
        disp(['Test index from ' num2str(indexStart), ' to ' num2str(indexEnd)]);
        
        dataLabTrain = select_index(dataLab, indexTrain);
        dataLabTest = select_index(dataLab, indexTest);
        
        nRep = round(pRep * round(dataUnlab.n/dataLabTrain.n));
        dataLabTrainRep = repepeat_data(dataLabTrain, nRep);
    end
    
    %% Generative model
    disp('===Generative Model====');
    [param, diff] = generative_model(dataLabTrainRep, dataUnlab, threshold);
    
    figure
    semilogy(diff)
    
    %% clasify
    disp('===Classification====');
    
    % build classifier
    lda = lda_train(param);
    
    % classify data
    yTest = lda_classify(lda, dataLabTest);
    yTrain = lda_classify(lda, dataLabTrain);
    yUnlab = lda_classify(lda, dataUnlab);

    % compute error    
    errorRateTest(i) = sum(yTest ~= dataLabTest.y)/dataLabTest.n;
    errorRateTrain(i) = sum(yTrain ~= dataLabTrain.y)/dataLabTrain.n;
    differenceMeRamon(i) = sum(yUnlab ~= yRamon)/(length(yUnlab));   
    
    % show results
    disp(['True error rate: ', num2str(errorRateTest(i))]);
    disp(['Train error rate: ', num2str(errorRateTrain(i))]);
    disp(['Ramon error rate: ', num2str(differenceMeRamon(i))]);
    disp(' ');
end

disp(['True error rate: ', num2str(mean(errorRateTest)), ' +- ' num2str(std(errorRateTest))]);
disp(['Train error rate: ', num2str(mean(errorRateTrain)), ' +- ' num2str(std(errorRateTrain))]);
disp(['Ramon error rate: ', num2str(mean(differenceMeRamon)), ' +- ' num2str(std(differenceMeRamon))]);