clear all
close all
clc
% plot the data

%% init
% set some settings
complete = true;
path = pwd;
figPath = [path, filesep,'fig', filesep];
dim = [200, 200, 1200, 500];

%% load data train
trainDataPath = [path, filesep, 'data', filesep, 'train.csv'];
testDataPath = [path, filesep, 'data', filesep, 'test.csv'];

dataLab = load_labeled_data(trainDataPath);
dataUnlab = load_unlabeled_data(testDataPath);

% plot means
plot_feat_mean(dataLab, false, dim);

% normalize
[dataLab, meanTrain, stdTrain] = normalize_data_struct1(dataLab);
[dataUnlab, meanTest, stdTest] = normalize_data_struct1(dataUnlab); 

% get some properties
get_properties(dataLab);

normality_test(dataLab, 1);
normality_test(dataLab, 2);

difference_test(dataLab, dataUnlab);

%% covariance default
dataLab1 = select_class(dataLab, 1);
dataLab2 = select_class(dataLab, 2);

covariance1 = cov(dataLab1.x);
covariance2 = cov(dataLab2.x);

figure('Position', [200,200, 1400, 600])
subplot(1,2,1)
cov_plot(covariance1, 1)

subplot(1,2,2)
cov_plot(covariance2, 2)

saveas(gcf, [figPath, 'cov'], 'epsc')

%% apply pca


%% covariance pca

%% scatter plot
% create prdataset
prdataLab = prdataset(dataLab.x , dataLab.y);
prdataLab = setprior(prdataLab,[0.5 0.5]);

figure
scatterd(prdataLab(:,[10,6]))

%% visualize mean
figure('Position', [200, 200, 600, 500])
plot(meanTrain, '*', 'Color', [1, 0.5, 0.5], 'Linewidth', 1)
hold on
plot(meanTest, '.', 'Color', [0,0,0])
xlim([1, length(meanTrain)])
ylim([-6, 4])
xlabel('feature')
ylabel('mean')
title('Mean value for different features')
legend('train data', 'test data')
saveas(gca, [figPath, 'mean'], 'epsc')

figure('Position', [200, 200, 600, 500])
plot(stdTrain, '*', 'Color', [1, 0.5, 0.5], 'Linewidth', 1)
hold on
plot(stdTest, '.', 'Color', [0,0,0])
xlim([1, length(meanTrain)])
xlabel('feature')
ylim([4.5, 7])
ylabel('standard deviation')
title('Standard Deviation for different features')
legend('train data', 'test data')
saveas(gca, [figPath, 'std'], 'epsc')

% plot some data
% plot()

%%
% kurtosis
% i1 = dataLab.y == dataLab.lab(1);
% i2 = dataLab.y == dataLab.lab(2);
% 
% dataLab1.x = dataLab.x(i1,:);
% dataLab1.y = dataLab.y(i1,:);
% dataLab1 = update_struct(dataLab1);
% 
% dataLab2.x = dataLab.x(i2,:);
% dataLab2.y = dataLab.y(i2,:);
% dataLab2 = update_struct(dataLab2);
% 
% %%
% [dataLab1N, meanX, stdX] = normalize_data_struct1(dataLab1);
% nFeat = size(dataLab1N.x,2)
% for feat = 1:nFeat
%     [h(feat), p(feat)] = kstest(dataLab1N.x(:,feat));
% end

% kLab = kurtosis(dataLab.x);
% kLabMean = mean(kLab);
% kLabStd = std(kLab);
% 
% kLab1 = kurtosis(dataLab1.x);
% kLabMean1 = mean(kLab1);
% 
% kLab2 = kurtosis(dataLab2.x);
% kLabMean2 = mean(kLab2);
% 
% kUnlab = kurtosis(dataUnlab.x);
% kUnlabMean = mean(kUnlab);
% kUnlabStd = std(kUnlab);
% 
% disp(['Mean kurtosis all labled: ', num2str(kLabMean), ' +- ', num2str(kLabStd)]);
% disp(['Mean kurtosis labled 1: ', num2str(kLabMean1)]);
% disp(['Mean kurtosis labled 2: ', num2str(kLabMean2)]);
% disp(['Mean kurtosis unlabeled: ', num2str(kUnlabMean), ' +- ', num2str(kUnlabStd)]);