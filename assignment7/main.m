clear all
close all
clc
% trains some classifiers on the labeld data set, and get an error on
% unseen samples from the same data set

% NOTE: we need to classify the unlabled NOISY data, so the achieved error
% does not give any guarantees for the actual error rate

%% init
% set some settings
complete = true;
nCrossVal = 5;
prwaitbar off
path = pwd;
figPath = [path, filesep,'fig', filesep];
% http://www.37steps.com/exam/sspca/html/sspca.html

% set classifier
c = {ldc, qdc, knnc, parzenc, svc, rbsvc, bpxnc};
cSting = ["ldc", "qdc", "knnc", "parzenc", "svc", "rbsvc", "bpxnc"];

pcaFracs = [0.6, 0.8, 0.9, 0.95, 0.9999999999];

%%
pcaString = "";
for pcaFrac = pcaFracs
    pcaString(end+1) = string(pcaFrac);
end

%% load data train
trainDataPath = [path, filesep, 'data', filesep, 'train.csv'];
testDataPath = [path, filesep, 'data', filesep, 'test.csv'];

% read csv
trainDataset = csvread(trainDataPath);
testDataset = csvread(testDataPath);

% split in actual data and labels
labeledDataStruct.x = trainDataset(:,2:end);
labeledDataStruct.y = trainDataset(:,1);
labeledDataStruct = update_struct(labeledDataStruct);

unlabeledDataStruct.x = testDataset;
unlabeledDataStruct.y = zeros(size(unlabeledDataStruct.x,1),1);
unlabeledDataStruct = update_struct(unlabeledDataStruct);

% plot means
plot_feat_mean(labeledDataStruct, false);

% normalize
[labeledDataStruct, meanTrain, stdTrain] = normalize_data_struct(labeledDataStruct);
[unlabeledDataStruct, meanTest, stdTest] = normalize_data_struct(unlabeledDataStruct); 

% create prdataset
dataLabeled = prdataset(labeledDataStruct.x , labeledDataStruct.y);
dataLabeled = setprior(dataLabeled,[0.5 0.5]);

% get some properties
get_properties(labeledDataStruct);

figure
scatterd(dataLabeled(:,[10,6]))
%% visualize mean
figure('Position', [200, 200, 600, 500])
plot(meanTrain, '*', 'Color', [1, 0.5, 0.5], 'Linewidth', 1)
hold on
plot(meanTest, '.', 'Color', [0,0,0])
xlim([1, length(meanTrain)])
ylim([-6, 4])
xlabel('feature')
ylabel('mean')
title('Mean value for different features')
legend('train data', 'test data')
saveas(gca, [figPath, 'mean'], 'epsc')

figure('Position', [200, 200, 600, 500])
plot(stdTrain, '*', 'Color', [1, 0.5, 0.5], 'Linewidth', 1)
hold on
plot(stdTest, '.', 'Color', [0,0,0])
xlim([1, length(meanTrain)])
xlabel('feature')
ylim([4.5, 7])
ylabel('standard deviation')
title('Standard Deviation for different features')
legend('train data', 'test data')
saveas(gca, [figPath, 'std'], 'epsc')



% plot some data
plot()

%% test
pcaCount = 0;
for pcaFrac = pcaFracs
    
    pcaCount = pcaCount + 1;
    
    map = pcam(pcaFrac);
    dataPCA = dataLabeled * map;
    disp(['features: ', num2str(size(dataPCA, 2))]);

    
    [error(pcaCount,:), std] = prcrossval(dataLabeled, map * c, nCrossVal);
end

%% plot
figure
bar(error)
set(gca, 'xtick', 1:length(c),'xticklabel',cSting)

legend(pcaString(2:end))
% disp(['error: ', num2str(error), ' +- ', num2str(std)]);
