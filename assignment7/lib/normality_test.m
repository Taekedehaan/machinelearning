function normality_test(data, class)

if class
    data = select_class(data, class);
end

[dataLab1N, ~, ~] = normalize_data_struct1(data);
nFeat = size(dataLab1N.x,2);
for feat = 1:nFeat
    [h(feat), p(feat)] = kstest(dataLab1N.x(:,feat));
    
    if h(feat)
        disp(['feature ', num2str(feat), ' is not normally distibuted, (p = ', num2str(p(feat)), ')']);
    end
end
if ~any(h)
    disp('==NORMALITY RESULTS==');
    disp(['all features from class ', num2str(class), ' are normally distributed at 5% significance']);
    disp(['mean p-value ',  num2str(mean(p))]);
    disp(['min m-value(p) ', num2str(min(p))]);
end

k = kurtosis(data.x);
kMean = mean(k);
kStd = std(k);

disp(['Mean kurtosis: ', num2str(kMean), ' +- ', num2str(kStd)]);