function [samples, nFeat] = get_properties(data)

[samples, nFeat] = size(data.x);
disp(['samples: ', num2str(samples)]);
disp(['features: ', num2str(nFeat)]);

end