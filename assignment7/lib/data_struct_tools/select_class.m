function data = select_class(data, class)
% selects all the data from a certain c;ass and outputs it as an updated
% struct

index = data.y == class;
data.x = data.x(index,:);
data.y = data.y(index,:);

data = update_struct(data);

end