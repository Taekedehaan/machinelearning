function data = select_index(data, index)
% selects all the data from a certain c;ass and outputs it as an updated
% struct

data.x = data.x(index,:);
data.y = data.y(index,:);

data = update_struct(data);

end