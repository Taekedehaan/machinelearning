function data = update_struct(data)
    data.lab = unique(data.y);

if ~any(data.lab == 0)
    data.lab = [data.lab; 0]';
elseif all(data.lab == 0)
else
    data.lab = sort(data.lab,'descend');
    data.lab(1:2) = sort(data.lab(1:2),'ascend');
    data.lab = data.lab';
end

data.n = size(data.y,1);
data.n1 = sum(data.y == 1);
data.n2 = sum(data.y == 2);

data.nFeat = size(data.x,2);
end