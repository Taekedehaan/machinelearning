function data = update_prior(data)
%Update prior of data set
n.total(1) = sum(data.y == data.lab(1));
n.total(2) = sum(data.y == data.lab(2));

data.prior(1) = sum(data.y == data.lab(1))/sum(n.total);
data.prior(2) = sum(data.y == data.lab(2))/sum(n.total);
end