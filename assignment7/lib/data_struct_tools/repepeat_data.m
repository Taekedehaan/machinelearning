function dataRep = repepeat_data(data, nRep)
dataRep.x = repmat(data.x, nRep, 1);
dataRep.y = repmat(data.y, nRep, 1);
dataRep = update_struct(dataRep);
end