function [data1, data2, meanX, stdX] = normalize_data_struct2(data1, data2)

x = [data1.x; data2.x]; 

stdX = std(x, 1);
meanX = mean(x, 1);

data1.x = (data1.x - meanX)./stdX;
data2.x = (data2.x - meanX)./stdX;