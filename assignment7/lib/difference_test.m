function difference_test(data1, data2)

% [dataLab1N, ~, ~] = normalize_data_struct1(data);
nFeat = size(data1.x,2);
for feat = 1:nFeat
    [h(feat), p(feat)] = kstest2(data1.x(:,feat), data2.x(:,feat));
    
    if h(feat)
        disp(['feature ', num2str(feat), ' are not from the same continuous distribution, (p = ', num2str(p(feat)), ')']);
    end
end

if ~any(h)
    disp('==NORMALITY RESULTS==');
    disp(['all features are from the same distribution at 5% significance']);
    disp(['mean p-value ',  num2str(mean(p))]);
    disp(['min m-value(p) ', num2str(min(p))]);
end
% 
% k = kurtosis(data.x);
% kMean = mean(k);
% kStd = std(k);
% 
% disp(['Mean kurtosis: ', num2str(kMean), ' +- ', num2str(kStd)]);
% 
% kstest2(x1,x2)