function cov_plot(data, class)
fontSize = 20;
colorMax = 0.4;
colorMin = 0.1;
spacing = 0.12;
font = 'Times New Roman';
font = 'Arial';

xLabel = 'features';
yLabel = 'features';

h = imagesc(data);
set(gca,'Ydir','Normal')

ax = gca;
ax.DataAspectRatio = [1 1 1];
c = colorbar;

% set(gca, 'Xtick', 1:size(data,2), 'XtickLabel', Cvec)
% set(gca, 'Ytick', 1:size(data,1), 'YtickLabel', Rvec)
% set(gca,'CLim',[colorMin,colorMax])

xlabel(xLabel)
ylabel(yLabel)
grid off
%% 
% Get the middle value of the color range
title(['Covariance matrix class ', num2str(class)])
set(gca,'FontSize',fontSize,'Fontname',font)


end