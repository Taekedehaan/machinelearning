function color = gen_colors(n)

c = (1:n)/n;
black = zeros(n, 3);
red = c' .* [ones(n,1 ), zeros(n,2)];
color = black + red;
end