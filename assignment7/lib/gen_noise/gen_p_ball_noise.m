function y = gen_p_ball_noise(r, p, dim)
% https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=758215
% http://mathworld.wolfram.com/DiskPointPicking.html
% https://math.stackexchange.com/questions/87230/picking-random-points-in-the-volume-of-sphere-with-uniform-probability

% generate random sample within sphere of radius r
n = dim(2);

% 1) Generate n independent random real scalars e ~G(1/p, p)
mean = 0; %p
sigma = sqrt(1/p);
a = 1/p;
c = p;

% point = normrnd(mean, sigma, dim);
% point = gg6(dim(1),dim(2),mean,scaleParam,shapeParam);
% point = normrnd(0, 1, dim);
z = gamrnd(a,1, dim);
point = z.^(1/c);

% 2) Construct the vector x
threshold = rand(dim);
sign = ones(dim);
sign(threshold > 0.5) = -1;
x = point .* sign;

% 3) Generate z
z = rand^(1/n);

% 4) get point
y = r * z * x / norm(x, p);