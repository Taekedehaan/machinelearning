function dataStruct = add_noise(dataStruct, p, r)

x = dataStruct.x;
feat = size(x,2);

for i = 1:dataStruct.n
   
    noise = gen_p_ball_noise(r, p, [1, feat]);    
    x(i,:) = x(i,:) + noise; 
end

dataStruct.x = x;

end

