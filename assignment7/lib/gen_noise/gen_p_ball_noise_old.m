function point = gen_p_ball_noise_old(r, p, dim)
% generate random sample within sphere of radius r

% http://mathworld.wolfram.com/DiskPointPicking.html
% https://math.stackexchange.com/questions/87230/picking-random-points-in-the-volume-of-sphere-with-uniform-probability

distance = r * rand^(1/dim(2));
point = normrnd(0, 1, dim);

point = distance * 1/(sum(abs(point).^p))^(1/p) * point;