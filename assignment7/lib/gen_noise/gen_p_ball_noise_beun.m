function point = gen_p_ball_noise_beun(r, p, dim)

point = inf(dim);
count = 0;

while sum(abs(point).^p)^(1/p) > 1
    point = rand(dim);
    count = count + 1;
end

sign = ones(dim);
sign(rand(dim) > 0.5) = -1;
point = sign .* point;

point = point * r;

disp(['Found solution in: ', num2str(count), ' itterations'])

end