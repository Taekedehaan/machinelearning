function [classifiedPercentage] = class_certainty(samples, yIn, lab)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
nClusterCurrent = size(samples,2);
nClasses = length(lab) - 1;

classifiedPercentage = zeros(nClasses, nClusterCurrent);

for i = 1:nClusterCurrent   
    sampleIndex = samples(~isnan(samples(:,i)),i);
    
    % loop over all classes (including none)
    for ii = 1:nClasses
        
        % Amount of samples from current class
        nCurrent = sum(yIn(sampleIndex) == lab(ii));
        
        % Amount of samples from other classes
        nOther = sum( (yIn(sampleIndex) ~= lab(end)) & (yIn(sampleIndex) ~= lab(ii)));
        
        if nOther
            
            % if there are other labels 
            classifiedPercentage(ii,i) = nCurrent/(nOther + nCurrent);
            
        % if there are smaples from current class, and none of others
        elseif nCurrent
            classifiedPercentage(ii,i) = 1;
            
        % if there are no labels from current class and no from others
        else
            classifiedPercentage(nClasses + 1,i) = 1;
        end
        %else 
    end
    % Determine fractions

end
end

