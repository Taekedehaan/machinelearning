function dataSum = sum_data(data, class)
index = data.y == class;
data1 = data.x(index,:);
dataSum = sum(data1, 1);
end