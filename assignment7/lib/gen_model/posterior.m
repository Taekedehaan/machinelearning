function [post1, post2] = posterior(data, param)
% c = 5;
% if all(size(param.sigma) == [2, 1])
%     param.sigma = repmat(param.sigma, 1, size(data.x,2));
% end

% p(x|1, sigma)
conditional1 = normpdf(data.x,param.mu(1,:),param.sigma(1,:)); %  * c
conditional1 = prod(conditional1,2);

% p(x|2, sigma)
conditional2 = normpdf(data.x,param.mu(2,:),param.sigma(2,:)); % * c
conditional2 = prod(conditional2,2);

% sum_y p(y) * p(x|y)
joint = param.P(1) * conditional1 + param.P(2) * conditional2;

post1 =  param.P(1) .* conditional1 ./ joint;
post2 =  param.P(2) .* conditional2 ./ joint;

% postTot1 = sum(post1 .* dataUnlab.x, 1);
% postTot2 = sum(post2 .* dataUnlab.x, 1);

% postProd1 = sum(post1 .* dataUnlab.x, 1);
% postProd2 = sum(post2 .* dataUnlab.x, 1);