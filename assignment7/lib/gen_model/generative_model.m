function [paramFinal, diff] = generative_model(dataLabTrain, dataUnlab, threshold)
%% init optimization
% we initialize training data
mean1 = mean(dataLabTrain.x(dataLabTrain.y == 1,:),1);
mean2 = mean(dataLabTrain.x(dataLabTrain.y == 2,:),1);

sigma1 = mean(std(dataLabTrain.x(dataLabTrain.y == 1,:),1));
sigma2 = mean(std(dataLabTrain.x(dataLabTrain.y == 2,:),1));

param(1).mu(1,:) = mean1;
param(1).mu(2,:) = mean2;
param(1).sigma(1,:) = sigma1;
param(1).sigma(2,:) = sigma2;
% param(1).sigma = ones(2,nFeat) + rand(2,nFeat)/10;
% param(1).sigma = ones(2,1) + rand(2,1)/10;

param(1).P = [0.5, 0.5];

t = 1;

while true

    %% update mean
    % get posterior
    [post1, post2] = posterior(dataUnlab, param(t));

    % COMPUTE NORMALIZATION
    normalize1 = sum(post1, 1) + dataLabTrain.n1;
    normalize2 = sum(post2, 1) + dataLabTrain.n2;
    
    % get sum values of labled data
    index = dataLabTrain.y == 1;
    dataLabTrain1 = dataLabTrain.x(index,:);
    totLab1 = sum(dataLabTrain1, 1);

    index = dataLabTrain.y == 2;
    dataLabTrain2 = dataLabTrain.x(index,:);
    totLab2 = sum(dataLabTrain2, 1);

    % update mu
    param(t+1).mu(1,:) = (sum(post1 .* dataUnlab.x, 1) + totLab1)/normalize1;
    param(t+1).mu(2,:) = (sum(post2 .* dataUnlab.x, 1) + totLab1)/normalize2;

    % compute distances
    distUnlab1 = sum((dataUnlab.x - param(t+1).mu(1,:)).^2,2);
    distUnlab2 = sum((dataUnlab.x - param(t+1).mu(2,:)).^2,2);
    distLab1 = sum((dataLabTrain1 - param(t+1).mu(1,:)).^2,2);
    distLab2 = sum((dataLabTrain2 - param(t+1).mu(2,:)).^2,2);

    partA1 = sum(post1 .* distUnlab1, 1)/(dataUnlab.nFeat * normalize1);
    partB1 = sum(distLab1,1)/(dataUnlab.nFeat * normalize1);

    partA2 = sum(post2 .* distUnlab2, 1)/(dataUnlab.nFeat * normalize2);
    partB2 = sum(distLab2,1)/(dataUnlab.nFeat * normalize2);

    % update sigma
    param(t + 1).sigma(1,:) = sqrt(partA1 + partB1);
    param(t + 1).sigma(2,:) = sqrt(partA2 + partB2);
    
    % update priors
    param(t + 1).P(1) = 1/(dataLabTrain.n + dataUnlab.n) * (sum(post1, 1) + dataLabTrain.n1);
    param(t + 1).P(2) = 1/(dataLabTrain.n + dataUnlab.n) * (sum(post2, 1) + dataLabTrain.n2);
    
    diff1 = sum((param(t).mu(1,:) - param(t+1).mu(1,:)).^2);
    diff2 = sum((param(t).mu(2,:) - param(t+1).mu(2,:)).^2);
    diff(t) = diff1 + diff2;
    
    if diff(t) < threshold
        break;
    end
    
    t = t + 1;
    
end
disp(['finished after ', num2str(t) , ' iterations']);

paramFinal = param(end);