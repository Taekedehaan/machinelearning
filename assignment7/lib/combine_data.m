function [dataFinal] = combine_data(dataCluster,data, weight)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

nAdd = ceil(weight * ( (dataCluster.n - data.n) / data.n)) - 1;

dataAdd.x = repmat(data.x, nAdd, 1);
dataAdd.y = repmat(data.y, nAdd, 1);

dataFinal.x = [dataCluster.x; dataAdd.x];
dataFinal.y = [dataCluster.y; dataAdd.y];

dataFinal = update_struct(dataFinal);
end

