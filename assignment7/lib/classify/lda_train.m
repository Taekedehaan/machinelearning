function lda = lda_train(param)

% x in omega_i if g_i(x) > g_j, ===> gij = g_i(x) - gj(x)
mu1 = param.mu(1,:);
mu2 = param.mu(2,:);

sigma1 = param.sigma(1,:);
sigma2 = param.sigma(2,:);

P1 = param.P(1);
P2 = param.P(2);

lda.w = mu1 - mu2;

lda.x0 = 1/2 * (mu1 + mu2) - sigma1* log(P1/P2) * (mu1 - mu2)/(sum((mu1 - mu2).^2));

