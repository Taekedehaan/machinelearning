function y = lda_classify(lda, dataLabTest)

% classify test set
g = lda.w * (dataLabTest.x - lda.x0)';

y(g > 0, 1) = 1;
y(g <= 0, 1) = 2;