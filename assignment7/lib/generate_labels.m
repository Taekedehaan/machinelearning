function [data] = generate_labels(data)

data.lab = unique(data.y);

if ~any(data.lab == 0)
    data.lab = [data.lab; 0]';
else
    data.lab = sort(data.lab,'descend');
    data.lab(1:2) = sort(data.lab(1:2),'ascend');
    data.lab = data.lab';
end



end