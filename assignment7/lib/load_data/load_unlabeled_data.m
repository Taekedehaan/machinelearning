function unlabledDataStruct = load_unlabeled_data(testDataPath)

testDataset = csvread(testDataPath);

unlabledDataStruct.x = testDataset;
unlabledDataStruct.y = zeros(size(unlabledDataStruct.x,1),1);
unlabledDataStruct = update_struct(unlabledDataStruct);