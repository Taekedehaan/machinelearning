function labeledDataStruct = load_labeled_data(labeledDataPath)

% read csv
labeledDataset = csvread(labeledDataPath);

% split in actual data and labels
labeledDataStruct.x = labeledDataset(:,2:end);
labeledDataStruct.y = labeledDataset(:,1);
labeledDataStruct = update_struct(labeledDataStruct);