function plot_feat_mean(data, applyPCA, dim)

if applyPCA
    % apply pca
    prData = prdataset(data.x , data.y);
    map = pcam(prData, 0.99999);
    prData = prData * map;

    data.x = prData.data;
    data.y = getlab(prData);
    data = update_struct(data);
end

% get mean class 1 and 2
labeledDataStruct1 = select_class(data, 1);
labeledDataStruct2 = select_class(data, 2);

[~, mean1, std1] = normalize_data_struct1(labeledDataStruct1);
[~, mean2, std2] = normalize_data_struct1(labeledDataStruct2); 

meanData = [mean1; mean2];
stdData = [std1; std2];

figPath = [pwd, filesep,'fig', filesep];

nClasses = size(meanData,1);
legendString = string(1:nClasses);

figure('Position', dim)
errorbar(meanData(1,:), stdData(1,:),'*', 'Color', [1, 0.5, 0.5], 'Linewidth', 1)
hold on
errorbar(meanData(2,:), stdData(2,:),'*', 'Color', [0, 0, 0], 'Linewidth', 1)

xlim([1, size(meanData,2)])
ylim([-15, 10])
xlabel('feature')
ylabel('mean \pm standard deviation')
title('Mean value \pm standard deviation for all features')
legend(legendString)
saveas(gca, [figPath, 'dataLab'], 'epsc')
saveas(gca, [figPath, 'dataLab'], 'fig')