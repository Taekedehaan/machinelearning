function [data, meanX, stdX] = normalize_data_struct1(data)

x = data.x; 

stdX = std(x, 1);
meanX = mean(x, 1);

data.x = (data.x - meanX)./stdX;