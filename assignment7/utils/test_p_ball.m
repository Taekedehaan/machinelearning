% test p ball noise generator
close all
clear all
clc

r = 1;
p = [0.5, 1, 100];
dim = [1, 2]; % 204
n = 500;

figPath = [pwd, filesep,'..', filesep,'fig', filesep];

figure('Position', [200, 200, 1200, 350])


for ip = 1:length(p)
    point = nan(n , dim(2));
    
    for i = 1:n
        point(i, :) = gen_p_ball_noise(r, p(ip), dim);
        % point(i, :) = gen_circle_noise(r, p, dim);
    end
    
    
%     k = kurtosis(point);
%     kMean = mean(k);
%     disp(['Mean kurtosis: ', num2str(kMean)]);
%     p = 9/kMean^2 + 1;
%     disp(['p: ', num2str(p)]);
    
    
    subplot(1,3,ip)
    plot(point(:,1), point(:,2), 'o', 'Color', [0, 0, 0])
    xlim([-r, r])
    ylim([-r, r])
    
    if ip == 2
        title('Generated pballs for different values of p')

    end
    
    if dim(2) > 2
        subplot(2,2,2)
        plot(point(:,1), point(:,3), 'o')
        xlim([-r, r])
        ylim([-r, r])
        
        
        subplot(2,2,3)
        plot(point(:,2), point(:,3), 'o')
        xlim([-r, r])
        ylim([-r, r])
        
        subplot(2,2,4)
        plot3(point(:,1), point(:,2), point(:,3), 'o')
        
    end
end

saveas(gca, [figPath, 'noisyBalls'], 'epsc')