A = gendatb([200 200]);
e1 = prcrossval(A,parzenc,8,1);
e5 = prcrossval(A,parzenc,8,5);
e25 = prcrossval(A,parzenc,8,25);
edps = prcrossval(A,parzenc,8,'DPS');
S = gendatb([1000,1000]);
etrue = S*(A*parzenc)*testc;
disp([etrue,e1,e5,e25,edps])