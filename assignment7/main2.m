clear all
close all
clc

%% init
disp('===INITIALIZING====')
% pr settings
complete = true;
prwaitbar off

% path
path = pwd;
figPath = [path, filesep,'fig', filesep];

% set classifier
c = {ldc, qdc, knnc, parzenc, svc, rbsvc, bpxnc};
cSting = ["ldc", "qdc", "knnc", "parzenc", "svc", "rbsvc", "bpxnc"];

% noise
r = 100;
p = 1;

% clustering
nUnlab = 1000;
classifiedThreshold = 0.6;
classifiedCertaintyThreshold = 0.2;
waitTime = 0;
vis = false;

%% load data train
pathLabData = [pwd, filesep, 'data', filesep, 'train.csv'];
pathUnlabData = [pwd, filesep, 'data', filesep, 'test.csv'];

dataLab = load_labeled_data(pathLabData);
dataUnlab = load_unlabeled_data(pathUnlabData);

% add noise
dataLab = add_noise(dataLab, p, r);

% normalize
[dataLab, dataUnlab, meanX, stdX] = normalize_data_struct(dataLab, dataUnlab);

% get some properties
[~, nFeat] = get_properties(dataLab);

% split labled data
[dataLabTrain, dataLabTest] = split_data(dataLab, 0.2);
% [dataUnlabTrain, dataUnlabTest] = split_data(dataUnlab, 0.5);

% combine to create test data set with missing labels
iRand = randperm(dataUnlab.n);
iRandTrain = iRand(1:nUnlab);

dataSemiLabTrain.x = [dataUnlab.x(iRandTrain,:); dataLabTrain.x]; % 
dataSemiLabTrain.y = [dataUnlab.y(iRandTrain,:);  dataLabTrain.y]; % 
dataSemiLabTrain = update_struct(dataSemiLabTrain);

%% create final trianing data set
% custer
disp('===CLUSTERING===')
[dataClusterTrain, nCluster] = cluster_custom(dataSemiLabTrain, complete, classifiedThreshold, classifiedCertaintyThreshold, waitTime, vis);

% show rersults
disp(['Number of clusters: ' num2str(nCluster)]);

i1 = dataClusterTrain.y == 1;
i2 = dataClusterTrain.y == 2;
i0 = dataClusterTrain.y == 0;
disp(['Number of samples Labled: ', num2str(sum(~i0))])
disp(['Number of samples class 1: ', num2str(sum(i1))]);
disp(['Number of samples class 2: ', num2str(sum(i2))]);
disp(['remained unlabled: ' num2str(sum(i0))])

% remove unlabled data
iDel = dataClusterTrain.y == 0;
dataClusterTrain.x(iDel,:) = [];
dataClusterTrain.y(iDel,:) = [];
dataClusterTrain = update_struct(dataClusterTrain);

% vombine cluster and uncluster
dataClusterTrain = combine_data(dataClusterTrain, dataLabTrain, 1);

%% pr data set
dataTrain = prdataset(dataClusterTrain.x, dataClusterTrain.y);
dataTest = prdataset(dataLabTest.x, dataLabTest.y);

dataTrain = setprior(dataTrain,[0.5 0.5]);
dataTest = setprior(dataTest,[0.5 0.5]);

%% test
disp('===start classifiaction===')
cTrained = dataTrain * c;
error = testc(dataTest, cTrained);

%% plot
figure
bar(cell2mat(error))

set(gca, 'xtick', 1:length(c),'xticklabel',cSting)
title(['Error rate using ', num2str(nUnlab), ' unlabled samples'])

ylabel('error rate')
xlabel('classifier')

saveas(gcf, [figPath, 'result'], 'epsc')
saveas(gcf, [figPath, 'result'], 'fig')
% legend(pcaString(2:end))
% disp(['error: ', num2str(error), ' +- ', num2str(std)]);
