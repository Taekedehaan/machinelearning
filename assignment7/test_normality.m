clear all
close all
clc

%% load data
pathLabData = [pwd, filesep, 'data', filesep, 'train.csv'];
pathUnlabData = [pwd, filesep, 'data', filesep, 'test.csv'];

dataLab = load_labeled_data(pathLabData);
dataUnlab = load_unlabeled_data(pathUnlabData);

% get class
dataLab1 = get_class(dataLab, dataLab.lab(1));
dataLab2 = get_class(dataLab, dataLab.lab(2));

%%
disp('===labled all====')
normality_test(dataLab)

disp('===labled 1===')
normality_test(dataLab1)

disp('===labled 2===')
normality_test(dataLab2)

disp('===unlabled===')
normality_test(dataUnlab)

