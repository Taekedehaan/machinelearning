function val = phi(s, center, width)
%The kernel for RBF network
%   input your action and state, it returns corresponing responses from all
%   the nodes

% init
n = length(center);
mu = center';


width = width * mean(diff(center));
std = width * ones(n,1);

% compute distance
eucl = (s - mu).^2;

% kernel
val = exp( -eucl./(2 * std.^2));

% normalize
for i = 1:length(s)
    if sum(val(:,i),1) ~= 0
        val(:,i) = val(:,i) ./ sum(val(:,i),1);
    else
        % warning('Failed to normalize');
    end
end
%val = [(1 - c0) * val; c0 * ones(1,length(s))];


