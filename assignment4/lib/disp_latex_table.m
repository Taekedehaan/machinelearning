function disp_latex_table(data)
%Show data in form which can easily be copied into a LateX table
%   Taeke de Haan
%   02-03-2018
disp( [repmat('& ',size(data,1),1), num2str(data, '%10.3f')]);
end

