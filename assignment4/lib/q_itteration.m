function Q = q_itteration(R, T, gamma)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

Q = zeros(2,6);
for i = 1:1000
    Qnew = zeros(2,6);
    
    % for all states
    for s = 1:6
        
        % for all actions
        for a = 1:2
            % R(s_new, s_old)
            
            %Qnew(a,s) = R(a,s) + gamma * max( Q(:, find(T(:, s, a))));
            Qnew(a,s) = T(:, s, a)' * R(:,s) + gamma * max( Q * T(:, s, a));
        end
    end
    Q = Qnew;
    %disp(Q)
    %disp_latex_table(Q)
    %pause;
end
end

