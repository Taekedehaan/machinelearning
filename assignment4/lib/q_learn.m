function [Q, s] = q_learn(Q, T, R, s, e, gamma, alpha)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    % reset when in termination state
    if s == 1 || s == 6
        s = floor(rand * 6) + 1;
    end
    
    % get action
    [~, a] = max(Q(:,s));
    if rand < e
        a = round(rand) + 1;
    end
    
    % apply action
    sPossible = find(T(:, s, a));
    
    if rand < T(sPossible(1), s, a)
        sNew = sPossible(1);
    else
        sNew = sPossible(end);
    end
    
    % update Q
    Q(a,s) = Q(a,s) +  alpha * (R(sNew,s) +   gamma * max( Q(:, sNew)) - Q(a, s));

    % update current state
    s = sNew;
end

