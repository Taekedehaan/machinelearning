clear all
close all
clc

%% init costants
computeDiff = true;
gamma = 0.5;        % Discount factor
alpha = 0.1;        % learning rate
e = 1;              % exploration rate
iterations = 1000; % number of itterations

% transition model
noiseVar = 0.01;
a = [-1, 1];        % possible action

% network design
nRBF = 6;            % number of basis functions
rbfC = linspace(1,6,nRBF);
rbfW = 1/10; %/10       % std of kernels, facotr mutiplied by the distance of the centers
n = length(rbfC);    % number of nodes

% visualization
ds = 0.01;         % incrementation step
states = 0:ds:7;    % states
color = get(gca,'colororder');
saveString = [num2str(length(rbfC)), '_', num2str(rbfW), '_', num2str(iterations)];
saveString(saveString == '.') = ',';
posDefault = 0.8 * [200,200, 875,656];
posFat = 0.8 * [200,200, 2*875,656];
% compute rbf
RBF = phi(states, rbfC, rbfW);

% init
s = 5 * rand + 1;
theta = zeros(size(phi(states, rbfC, rbfW),1),2);
count = 0;

thetaDist = zeros(size(theta));
diff = nan(iterations,1);

% init Q
Qinit(1,:) = theta(:,1)' * phi(states, rbfC, rbfW); % initial Q left
Qinit(2,:) = theta(:,2)' * phi(states, rbfC, rbfW); % initial Q right

while count < iterations
    count = count + 1;
    
    % reset position if required
    if s < 1.5 || s >= 5.5
        s = 5 * rand + 1; %randi(sInitRange);
    end
    
    % determine if we want to explore
    if e > rand
        aI = round(rand) + 1;
    else
        [~, aI] = max(phi(s,rbfC, rbfW)' * theta);   
    end
    
    % apply chosen action
    if max(phi(s, rbfC, rbfW)) == 0
        sNew = s;
    elseif s < 1.5 || s >= 5.5
        % we cannot move in a tremination state
        sNew = s;
    else
        sNew = s + a(aI) + normrnd(0, sqrt(noiseVar));
    end
    
    % give reward if robot deserves it
    if sNew < 1.5 && s > 1.5
        R = 1;
    elseif sNew >= 5.5 && s < 5.5
        R = 5;
    else
        R = 0;
    end
    
    % Q plus
    Qplus = R + gamma *(max(phi(sNew, rbfC, rbfW)' * theta));

    % Gradient decent
    theta(:,aI) = theta(:,aI) + alpha * (Qplus - phi(s, rbfC, rbfW)' * theta(:,aI)) * phi(s, rbfC, rbfW);
    s = sNew;
    
    % Compute difference
    diff(count) = sum(sqrt(sum((theta - thetaDist).^2,1)),2);
end

%% determine final Q
Q(1,:) = theta(:,1)' * phi(states, rbfC, rbfW); % initial Q left
Q(2,:) = theta(:,2)' * phi(states, rbfC, rbfW); % initial Q right

%% Plot difference
title('Convergence of the RBFN')
figure('Position', posDefault)
plot(diff)
xlabel('Itterations');
ylabel('Difference')
title('Norm 2 distance from \theta to 0')
saveas(gca,[ 'fig/RBFN_', saveString, '_convergence'], 'jpg')
saveas(gca,['fig/RBFN_', saveString, '_convergence'], 'epsc')

%% Plot initial Q
figure('Position', posDefault)
plot(states, Qinit(1,:))
hold on
plot(states, Qinit(2,:))
legend('left', 'right','Location','northwest')

%% RBF
figure('Position', posFat)
plot(states, RBF')
xlabel('state')
ylabel('activation')
title('Radial Basis Functions')

saveas(gca,[ 'fig/RBFN_', saveString, '_RBF'], 'jpg')
saveas(gca,['fig/RBFN_', saveString, '_RBF'], 'epsc')

%% plot Finmal Q
ampl = 6;
figure('Position', posDefault)
hold on
h(1) = area(states, ampl * (Q(1,:) > Q(2,:)));
h(2) = area(states, ampl * (Q(1,:) < Q(2,:)));
h(3) = plot(states, Q(1,:), 'Color', color(1,:));
h(4) = plot(states, Q(2,:), 'Color', color(2,:));

alpha = 0.9;
color1 = color(1,:) + (1 - color(1,:)) * alpha;
color2 = color(2,:) + (1 - color(2,:)) * alpha;

h(1).FaceColor = color1;
h(1).EdgeColor = color1;
h(2).FaceColor = color2;
h(2).EdgeColor = color2;

grid minor
set(gca,'layer','top');

axis([1, 6, 0 , ampl])
legend([h(3) h(4)], 'left', 'right','Location','northwest')

xlabel('state')
ylabel('Q')
title('Q-function')

saveas(gca,[ 'fig/RBFN_', saveString, '_Q'], 'jpg')
saveas(gca,['fig/RBFN_', saveString, '_Q'], 'epsc')