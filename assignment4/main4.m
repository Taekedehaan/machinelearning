clear all
close all
clc

R = zeros(6, 6); %R(s_new, s_old)
R(6, 1:5) = 5; 
R(1, 2:6) = 1;

gamma = 0.5; % discount factor
explore = 0:0.25:1;
alpha = 0:0.25:1; % learning rate
itterations = 500;

fontSize = 20;
lineWidth = 2;
pFail = 0;

% init error
error = nan(itterations, length(explore));

% actions
Tleft = [1, 1, 0, 0, 0, 0;
    0, 0, 1, 0, 0, 0;
    0, 0, 0, 1, 0, 0;
    0, 0, 0, 0, 1, 0;
    0, 0, 0, 0, 0, 0;
    0, 0, 0, 0, 0, 1];

Tright = [ 1, 0, 0, 0, 0, 0;
    0, 0, 0, 0, 0, 0;
    0, 1, 0, 0, 0, 0;
    0, 0, 1, 0, 0, 0;
    0, 0, 0, 1, 0, 0;
    0, 0, 0, 0, 1, 1];

T = cat(3,Tleft, Tright);

% determine true Q_star
Q_true = q_itteration(R, T, gamma);



% apply Q learn
for iAlpha = 1:length(alpha)
    for iExplore = 1:length(explore)
        % init Q
        Q = zeros(2,6);

        % init state
        s = floor(rand * 6) + 1;

        for i = 1:itterations
            [Q, s] = q_learn(Q, T, R, s, explore(iExplore), gamma, alpha(iAlpha));
            error(i,iExplore, iAlpha) = sqrt(sum(sum((Q_true - Q).^2)));    
        end
    end
end

% show result
disp_latex_table(Q)

%% plot result exploration alpha
figure('Position', [200, 200, 600, 600])

plot(error(:,:,round(length(alpha)/2)), 'Linewidth', lineWidth)
legend([repmat('e = ',length(explore),1), num2str(explore')])

title(['Error with learning rate =', num2str(alpha(round(length(alpha)/2)))])
xlabel('itteration')
ylabel('error')
grid minor
set(gca,'Fontsize',fontSize)
axis([0 itterations, 0, 7])

saveas(gca, 'fig/error_explr', 'jpg')
saveas(gca, 'fig/error_explr', 'epsc')


%% plot result alpha
figure('Position', [200, 200, 600, 600])

plot(permute(error(:,round(length(explore)/2),:),[1, 3, 2]), 'Linewidth', lineWidth)
legend([repmat('\alpha = ',length(alpha),1), num2str(alpha')])


title(['Error with exploration rate = ', num2str(explore(round(length(explore)/2)))])
xlabel('itteration')
ylabel('error')
grid minor
set(gca,'Fontsize',fontSize)
axis([0 itterations, 0, 7])

saveas(gca, 'fig/error_alpha', 'jpg')
saveas(gca, 'fig/error_alpha', 'epsc')