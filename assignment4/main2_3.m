clear all
close all
clc

R = zeros(6, 6); %R(s_new, s_old)
R(6, 1:5) = 5; 
R(1, 2:6) = 1;

gamma = 0.5;

Tleft = [1, 1, 0, 0, 0, 0;
    0, 0, 1, 0, 0, 0;
    0, 0, 0, 1, 0, 0;
    0, 0, 0, 0, 1, 0;
    0, 0, 0, 0, 0, 0;
    0, 0, 0, 0, 0, 1];

Tright = [ 1, 0, 0, 0, 0, 0;
    0, 0, 0, 0, 0, 0;
    0, 1, 0, 0, 0, 0;
    0, 0, 1, 0, 0, 0;
    0, 0, 0, 1, 0, 0;
    0, 0, 0, 0, 1, 1];

T = cat(3,Tleft, Tright);


% determine true Q_star
Q = q_itteration(R, T, gamma);

disp_latex_table(Q)