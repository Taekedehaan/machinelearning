function s = hausdorf_obs(A, B, k, type)
% returns hausdorf distance based on the euclidean distance.
%   A, B: input vectors, with dim [instances x features]  
%   s: hausdorf distance

switch type
    case 'max'
        type = 'descend';
    case 'min'
        type = 'ascend';
end

% compute distance matrix
dist = pdist2(A, B,'euclidean'); %[nA x nB]

%% max_a min_b (dist)
% determined for which b distance is minumum
[~, minIndex] = min(dist(:));
[~, iB] = ind2sub(size(dist), minIndex);

[value, ~] = sort(dist(:,iB), type);
s1 = value(k);

%% max_b min_a (dist)
% determined for which a distance is minumum
[~, minIndex] = min(dist(:));
[iA, ~] = ind2sub(size(dist), minIndex);

[value, ~] = sort(dist(iA,:), type);
s2 = value(k);

s = max([s1, s2]);
end