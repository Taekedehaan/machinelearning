function m = compute_distance_obs(dataBags, instances, label, sigma)
% init
nInstances = size(instances,1);
nBags = dataBags.n;
m = nan(nInstances, nBags);
count = 0;
textprogressbar('Computing distance: ');

% compute
for bag = 1:nBags
    s = nan(nInstances,1);
    
    for k = 1:nInstances
        count = count + 1;
        textprogressbar(count/(nBags*nInstances) * 100);
        
        index = bag == label;
        
        xBag = instances(index,:);
        xCurr = instances(k,:);
        
        dist = sum((xBag - xCurr).^2,2);
        s(k) = max(exp(-dist/sigma^2));
    end
    
    m(:,bag) = s;
end

textprogressbar('done:');