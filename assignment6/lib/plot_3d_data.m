function f = plot_3d_data(data, figureDim, varargin)

% check if input is okay
if length(varargin) > 1
    error('Too many input arguments');
end


labelsTrue = data.labels;

% plot dfata
dataPlot1 = data.data(labelsTrue == 1,:);
dataPlot2 = data.data(labelsTrue == 2,:);
labelsPlot = ['r', 'g', 'b'];

f = figure('Position', figureDim);
subplot(2,2,1)
hold on
plot(dataPlot1(:,1), dataPlot1(:,2), 'o', 'linewidth', 1)
plot(dataPlot2(:,1), dataPlot2(:,2), 'o', 'linewidth', 1)
xlabel(labelsPlot(1));
ylabel(labelsPlot(2))
axis([0, 255, 0, 255])

subplot(2,2,2)
hold on
plot(dataPlot1(:,2), dataPlot1(:,3), 'o', 'linewidth', 1)
plot(dataPlot2(:,2), dataPlot2(:,3), 'o', 'linewidth', 1)
xlabel(labelsPlot(2));
ylabel(labelsPlot(3))
axis([0, 255, 0, 255])

subplot(2,2,3)
hold on
plot(dataPlot1(:,1), dataPlot1(:,3), 'o', 'linewidth', 1)
plot(dataPlot2(:,1), dataPlot2(:,3), 'o', 'linewidth', 1)
xlabel(labelsPlot(1));
ylabel(labelsPlot(3))
axis([0, 255, 0, 255])

subplot(2,2,4)
hold on
plot3(dataPlot1(:,1), dataPlot1(:,2), dataPlot1(:,3), 'o', 'linewidth', 1)
plot3(dataPlot2(:,1), dataPlot2(:,2), dataPlot2(:,3), 'o', 'linewidth', 1)
xlabel(labelsPlot(1));
ylabel(labelsPlot(2));
zlabel(labelsPlot(3));
axis([0, 255, 0, 255, 0, 255])
legend('apple', 'banana')

% if seed is given apply it
if ~isempty(varargin)
    saveID = varargin{1};
    saveas(f, saveID, 'epsc')
    saveas(f, saveID, 'fig')
end
