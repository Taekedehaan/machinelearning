function m = compute_distance2(dataBagsFrom, dataBagsTo, sigma)
%% conmuptes data distance from dataBagsFrom, to dataBagsTo
% - dataBagsFrom: data bags from where the distance is computed, e.g.
%   testBag
% - dataGabsTo: data bags to which the distance is computed, e.g. trainBag
% - label: 
% - sigma: a hyper parameter


% unpack instances
[instancesFrom, nInstancesFrom, labelInstancesFrom] = unpack_instances(dataBagsFrom);
[instancesTo, nInstancesTo, labelInstancesTo] = unpack_instances(dataBagsTo);

% init
nBagsTo = dataBagsTo.n;
m = nan(nInstancesFrom, nBagsTo);
textprogressbar('Computing distance: ');

% compute
for bag = 1:nBagsTo   
    textprogressbar(bag/(nBagsTo) * 100);
    index = bag == labelInstancesTo;
    
    xBag = instancesTo(index,:);
    xCurr = instancesFrom;
    
    dist = pdist2(xCurr, xBag,'squaredeuclidean');      
    m(:,bag) = max(exp(-dist/sigma^2) , [], 2);
end

textprogressbar('done')