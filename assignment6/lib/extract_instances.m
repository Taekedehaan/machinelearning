function dataBags = extract_instances(dataInstances, data)

images = data.x;
indexInstances = dataInstances.x;

objects = 1:data.n;
colors = 1:3;

for obj = objects
    clear meanValue

    indexInstance = cell2mat(indexInstances(obj));
    uniqueValues = unique(indexInstance);
    image = cell2mat(images(obj));

    for uniqueValue = uniqueValues(:)'
        mask = indexInstance == uniqueValue;

        for color = colors(:)'
            imageColor = image(:,:,color);
            value = imageColor(mask);
            meanValue(uniqueValue, color) = mean(value);
        end
    end

    instances(obj) = {meanValue};
end

dataBags = data;
dataBags.x = instances;

end