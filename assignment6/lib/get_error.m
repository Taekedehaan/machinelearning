function [errorRate, missApple, missBanana] = get_error(data, trainedFischer)

bagid = getident(data,'milbag');
nBags = length(unique(bagid));

labelsClassified = nan(nBags,1);
labelsTrue = nan(nBags,1);

% loop over bags
count = 0;
for bag = unique(bagid)'
    
    count = count + 1;
    i = bag == bagid;

    dataCurrent = data.data(i,:);
    labelBag = unique(data.labels(i,:));
    
    if length(labelBag) > 1
        warning('Multiple labels in this bag!');
    end

    result = dataCurrent * trainedFischer;
    [~, labels] = max(result.data,[],2);

    labelsClassified(count) = combine_instance_labels(labels);
    labelsTrue(count) = labelBag;
end

correct = labelsClassified == labelsTrue;
errorRate = 1 - sum(correct)/nBags;

missApple = sum(~correct(labelsTrue == 1));
missBanana = sum(~correct(labelsTrue == 2));

end