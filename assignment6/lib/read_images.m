function data = read_images(imageDir)

textprogressbar('Opening images...');
pathCurrent = pwd;

imageDirPath = [pathCurrent, filesep, imageDir];
Contents = dir(imageDirPath);

countObj = 0;
countClass = 0;

for class = 3:(length(Contents))
    
    countClass = countClass + 1;
    subDirCurr = Contents(class).name;
    
    imapeSubDirPath = [imageDirPath, filesep, subDirCurr];
    Image = dir(imapeSubDirPath);
    
    imagecount = 0;
    for object = 3:length(Image)
        countObj = countObj + 1;
        
        textprogressbar(countObj / 120 * 100);
        % waitbar(countObj / 120, f,['substacting class ', subDirCurr]);
        
        imageName = Image(object).name;
        
        imagecount = imagecount + 1;
        images(countObj, 1) = {imread([imapeSubDirPath, filesep, imageName])};
        labels(countObj, 1) = countClass;
    end
end

textprogressbar('done')

data.x = images;
data.y = labels;
data = update_struct(data);
end