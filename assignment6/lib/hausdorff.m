function s = hausdorff(A, B, k)
% returns hausdorf distance based on the euclidean distance.
%   A, B: input vectors, with dim [instances x features]
%   s: hausdorf distance

type = 'ascend';

% compute distance matrix
dist = pdist2(A, B,'euclidean'); %[nA x nB]

% max_a min_b (dist)
% determined for which b distance is minumum
s1 = get_hasusdorf_dist(dist, k, type);

% max_b min_a (dist)
% determined for which a distance is minumum
s2 = get_hasusdorf_dist(dist', k, type);

% get actual hausdorff distance
s = max([s1, s2]);

function s = get_hasusdorf_dist(dist, k, type)
    % computes one of the 2 possible hausdord distances
    [~, minIndex] = min(dist(:));
    [~, iB] = ind2sub(size(dist), minIndex);

    [value, ~] = sort(dist(:,iB), type);
    s = value(k);
end

end