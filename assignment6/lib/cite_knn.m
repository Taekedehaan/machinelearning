function error = cite_knn(dataTrain, dataTest, R, C, k, type)
% classifies data Test and returns the error

nDataTest = dataTest.n;
nDataTrain = dataTrain.n;

%% get distance matrix
% in from [datatrain x data test]
dist = nan(nDataTrain, nDataTest);

for ii = 1:nDataTrain
    for i = 1:nDataTest
        dist(ii,i) = hausdorff(dataTrain.x{ii},dataTest.x{i}, k);
    end
end

% determine which type is selected
switch type
    case 'closeRef'
        refType = 'ascend';
    case 'farRef'
        refType = 'descend';
end

%% get references
% for each test sample which training sample is the closest
[~, index] = sort(dist, 1, refType);
references = index(1:R,:)';
refCell = mat2cell(references,ones(60,1));

%% get cites
% for each train sample which test sample is the closest

% get C nearest test samples
[~, index] = sort(dist, 2, refType);
cite = index(:,1:C);

% connect the train smaples to corresponing test sample
for i = 1:nDataTest
    trainBagsIndex = find(cite == i);
    [row, ~] = ind2sub(size(cite), trainBagsIndex);
    citeCell{i} = row';
end

%% combine references and cites
for i = 1:nDataTest
     total{i} = [refCell{i}, citeCell{i}];
end

%% classify
for i = 1:nDataTest
     score(i)  = sum((dataTrain.y(total{i}) - 1))/(length(total{i}));
end

% depending on the type of knn classify the the selected or opposite class
switch type
    case 'closeRef'
        lab(score > 0.5, :) = dataTrain.lab(2);
        lab(score <= 0.5, :) = dataTrain.lab(1);
    case 'farRef'
        lab(score > 0.5, :) = dataTrain.lab(1);
        lab(score <= 0.5, :) = dataTrain.lab(2);
end

% compute and show error
error = 1 - sum(lab == dataTest.y)/nDataTest;
disp(error);