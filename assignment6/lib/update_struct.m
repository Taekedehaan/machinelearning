function data = update_struct(data)
    data.lab = sort(unique(data.y));
    data.n = size(data.y,1);
end