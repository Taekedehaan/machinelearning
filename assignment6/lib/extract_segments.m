function dataOut = extract_segments(data, width)

images = data.x;
objects = 1:data.n;

textprogressbar('Extraxting instances: ');

for object = objects
    image = cell2mat(images(object));

    imageMeanShift = im_meanshift(image, width);
    uniqueValues = unique(imageMeanShift);
    
    % feedback
    textprogressbar(object/data.n * 100);
    % disp(['unique numbers: ', num2str(length(uniqueValues)')]);
    locationInstances(object, 1) = {imageMeanShift};
end

textprogressbar('done');

dataOut = data;
dataOut.x = locationInstances;
end