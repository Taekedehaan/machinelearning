function [dataTest, dataTrain] = split_data(data, pTrain, varargin)
% split the data set into 2 parts, where the training set size is in
% accordance with the desired faction or number as input.

% check if input is okay
if length(varargin) > 1
    error('Too many input arguments');
end

% if seed is given apply it
if ~isempty(varargin)
    rng(varargin{1});
end

% data size
nData = data.n;
nData1 = sum(data.y == data.lab(1));
nData2 = sum(data.y == data.lab(2));

% size of training pool
if pTrain >= 1
    nTrain = pTrain;
else
    nTrain = round(pTrain * nData / 2);
end

%create randowm vectors for both classes
iRandom1 = randperm(nData1);
iRandom2 = randperm(nData2);


% indexes corresponding to both classes
iClass1 = find(data.y == data.lab(1));
iClass2 = find(data.y == data.lab(2));

% suffle, and pick first n values
iTrain1 = iClass1(iRandom1(1:nTrain));
iTrain2 = iClass2(iRandom2(1:nTrain));

% the remaining values will be the test set
iTest1 = iClass1(iRandom1(nTrain + 1:end));
iTest2 = iClass2(iRandom2(nTrain + 1:end));

% now combine
iTest = [iTest1(:); iTest2(:)];
iTrain = [iTrain1(:); iTrain2(:)];

% a small test to determine whehter training samples ended up in the test set
for i = iTest(:)'
    if any(i == iTrain)
        warning('An index ended up in the test and train set!');
    end
end

% selec test data
dataTest.x = data.x(iTest);
dataTest.y = data.y(iTest);

% select train data
dataTrain.x = data.x(iTrain);
dataTrain.y = data.y(iTrain);

% check whether we lost any data
if length(iTest) + length(iTrain) ~= nData
    warning('problem with test and train data size');
    disp(['length test: ', num2str(length(iTest)), 'length train: ', num2str(length(iTrain))]);
end

% also copy labels
dataTest = update_struct(dataTest);
dataTrain = update_struct(dataTrain);
end

