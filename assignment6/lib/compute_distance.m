function m = compute_distance(dataBags, instances, label, sigma)


% init
nInstances = size(instances,1);
nBags = dataBags.n;
m = nan(nInstances, nBags);
textprogressbar('Computing distance: ');

% compute
for bag = 1:nBags   
    textprogressbar(bag/(nBags) * 100);
    index = bag == label;
    
    xBag = instances(index,:);
    xCurr = instances;
    
    dist = pdist2(xCurr, xBag,'squaredeuclidean');      
    m(:,bag) = max(exp(-dist/sigma^2) , [], 2);
end

textprogressbar('done');