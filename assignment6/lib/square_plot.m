function square_plot(errorMean, errorSTD, Rvec, Cvec, k)
fontSize = 20;
colorMax = 0.4;
colorMin = 0.1;
spacing = 0.12;
font = 'Times New Roman';
font = 'Arial';

xLabel = 'cites';
yLabel = 'references';

figure('Position', [200,200, 700, 600])
h= imagesc(errorMean(:,:,k));
ax = gca;
ax.DataAspectRatio = [1 1 1];
c = colorbar;

set(gca, 'Xtick', 1:size(errorMean,2), 'XtickLabel', Cvec)
set(gca, 'Ytick', 1:size(errorMean,1), 'YtickLabel', Rvec)
set(gca,'CLim',[colorMin,colorMax])

xlabel(xLabel)
ylabel(yLabel)
title(['error for k = ' num2str(k)])
grid off
%% 
% Get the middle value of the color range
midValue = colorMax/2; 

% Choose white or black for the text color of the strings so they can be easily seen over the background color
textColors = errorMean < midValue;  
for x = 1:size(errorMean,2)
    for y = 1:size(errorMean,1)
        text(x, y-spacing, num2str(errorMean(y, x, k),'%3.2f'),'HorizontalAlignment','center','VerticalAlignment','middle','Color',repmat(textColors(y, x, k),1,3),'FontSize', fontSize,'Fontname',font);
        text(x, y+spacing, ['SD=', num2str(errorSTD(y, x, k),'%3.2f')],'HorizontalAlignment','center','VerticalAlignment','middle','Color',repmat(textColors(y, x, k),1,3),'FontSize', fontSize-10,'Fontname',font);
    end
end

set(gca,'FontSize',fontSize,'Fontname',font)
hold on

% plot lines
for i = 1:(size(errorMean, 1) + 1)
    plot([.5,21.5],[i-.5,i-.5],'k-', 'Linewidth', 1); % horizontal
end

for i = 1:(size(errorMean, 2) + 1)
    plot([i-.5,i-.5],[.5,21.5],'k-', 'Linewidth', 1); % vertical
end
end