function dataImages = update_image(dataInstances, dataBags)

locationInstances = dataInstances.x;
instances = dataBags.x;

objects = 1:dataBags.n;
colors = 1:3;


for obj = objects

    % get current obj
    locationInstance = cell2mat(locationInstances(obj));
    instance = cell2mat(instances(obj));

    % reset image
    image = nan([size(locationInstance),3]);

    for i = 1:size(instance,1)
        mask = locationInstance() == i;
        index = find(mask);

        for color = colors
            image(index) = instance(i, color);
            index = index + numel(locationInstance);
        end

    end

    images(obj) = {image};
end

dataImages = dataBags;
dataImages.x = images;
end