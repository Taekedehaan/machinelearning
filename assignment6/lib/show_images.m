function f =show_images(data, figureDim, varargin)

% check if input is okay
if length(varargin) > 1
    error('Too many input arguments');
end

images = data.x;

nTot = data.n;
nRows = round(nTot/4);
nCols = ceil(nTot/nRows);

dim = length(size(cell2mat(images(1,1))));

f = figure('Position', figureDim);
count = 0;
for object = 1:nTot
    count = count + 1;

    subplot(nRows, nCols, count);

    imShow = cell2mat(images(object));

    if dim == 2
        imShow = mat2gray(imShow, [min(imShow(:)), max(imShow(:))]) * 255;
    end
    imshow(uint8(imShow));
end

% if seed is given apply it
if ~isempty(varargin)
    saveID = varargin{1};
    saveas(f, saveID, 'epsc')
    saveas(f, saveID, 'fig')
end
