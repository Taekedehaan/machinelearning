function [instances, nInstances, label] = unpack_instances(data)

% totan number of instance
bags = 1:data.n;


nInstances = 0;
for bag = bags
    
    currentInstances = cell2mat(data.x(bag));
    nInstancesCurr = size(currentInstances,1);
    
    index = (nInstances + 1):(nInstances + nInstancesCurr);
 
    instances(index, 1:3) = currentInstances;
    label(index, 1) = bag;
    nInstances = nInstances + nInstancesCurr;
end