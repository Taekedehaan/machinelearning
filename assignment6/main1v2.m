% reset
clear all
close all
clc

%% init
figureDim = [100, 100, 1150, 400];
figureDim2 = [100, 100, 1150, 800];
figDir = [pwd, filesep, 'fig', filesep];
imageDir = 'sival_apple_banana';
nPlot = 4;
scale = 0.1;
randomSeed = 1;
nError = 10;
sigma = 50;
widths = [5, 7, 10, 20, 30, 40, 50]; %[3, 5
classifierName = 'miles';

% load images
data = read_images(imageDir);

% rescale images
data = scale_images(data, scale);

%% preporcess
[~, dataPlot] = split_data(data, nPlot, randomSeed);
show_images(dataPlot, figureDim);

%% loop over width
% init loop
count = 0;

% save memory
nInstances = nan(length(widths),1);
errorRateTrain = nan(length(widths),1);
errorRateTrueMean = nan(length(widths),1);

for width = widths
    disp(['===', num2str(width), '==='])
    saveKey = num2str(width);
    count = count + 1;
    
    %% gen features
    % extract segments
    dataSegments = extract_segments(data, width);
    
    [~, dataPlot] = split_data(dataSegments, nPlot, randomSeed);
    show_images(dataPlot, figureDim, [figDir, 'img_def', saveKey]);
    
    % generate bags
    dataBags = extract_instances(dataSegments, data);
    
    % unpack instances
    [instances, nInstances(count), labelInstances] = unpack_instances(dataBags);
    
    % reconstruc images
    dataImagesMeanShift = update_image(dataSegments, dataBags);
    [~, dataPlot] = split_data(dataImagesMeanShift, nPlot, randomSeed);
    show_images(dataPlot, figureDim, [figDir, 'img_seg', saveKey]);
    
    %% ===test===
    % result1 = compute_distance(dataBags, instances, labelInstances, sigma);
    % result2 = compute_distance2(dataBags, dataBags, sigma);
    % disp(sum(abs(result1(:) - result2(:))));
    
    % textprogressbar('Classifying data: ');
    errorRateTrain = nan(1, nError);
    errorRateTrue = nan(1, nError);
        
    for i = 1:nError
        % split into train and test set
        [bagsTrain, bagsTest] = split_data(dataBags, 0.5);

        % store data in prtools
        % Compute distances, and store in prdata set
        mTrain = compute_distance2(bagsTrain, bagsTrain, sigma);
        mTest = compute_distance2(bagsTrain, bagsTest, sigma);
        prTest = prdataset(mTrain',bagsTrain.y);
        prTrain = prdataset(mTest',bagsTest.y);

        % set priors
        prTrain = setprior(prTrain, [0.5, 0.5]);
        prTest = setprior(prTest, [0.5, 0.5]);

        % classify
        errorRateTrain(i) = testc(prTrain, prTrain * liknonc);
        errorRateTrue(i) = testc(prTest, prTrain * liknonc);
    end
    
    errorRateTrueMean(count) = mean(errorRateTrue);
    errorRateTrueStd(count) = std(errorRateTrue);
    
    errorRateTrainMean(count) = mean(errorRateTrain);
    errorRateTrainStd(count) = std(errorRateTrain);
    
    disp(['Number of instances: ', num2str(nInstances(count))]);
    disp(['Train error rate: ', num2str(errorRateTrainMean(count))]);
    disp(['True error rate: ', num2str(errorRateTrueMean(count))]);
    
    close all
end

%% plot result
figure('Position', figureDim)
hold on

errorbar(widths, errorRateTrueMean, errorRateTrueStd, 'Linewidth', 2)
errorbar(widths, errorRateTrainMean, errorRateTrainStd, 'Linewidth', 2)

legend('True error', 'Train error')
xlabel('width')
ylabel('error rate')
title('Error Rate For Different Widths')
ylim([0,0.3])
saveas(gcf, [figDir, 'error_rate_miles'], 'epsc');
saveas(gcf, [figDir, 'error_rate_miles'], 'fig');