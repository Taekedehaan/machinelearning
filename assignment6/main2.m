% reset
clear all
close all
clc

%% init
disp('MILES')

figureDim = [100, 100, 1150, 400];
figureDim2 = [100, 100, 1150, 800];
figDir = [pwd, filesep, 'fig', filesep];
imageDir = 'sival_apple_banana';
nPlot = 4;
scale = 0.1;
randomSeed = 1;
nError = 20;
sigma = 10;
widths = [7, 10, 20, 30, 40, 50]; %[3, 5
pcaDims = [5, 7, 10, 15, 20, 30];
c = qdc; %liknonc;
% load images
data = read_images(imageDir);

% rescale images
data = scale_images(data, scale);

%% preporcess
[~, dataPlot] = split_data(data, nPlot, randomSeed);
show_images(dataPlot, figureDim);

%% loop over width
% init loop
count = 0;

% save memory
nInstances = nan(length(widths),1);
errorRateTrain = nan(length(widths),1);
errorRateTrueMean = nan(length(widths),1);

for width = widths
    disp(['===', num2str(width), '==='])
    saveKey = num2str(width);
    count = count + 1;
    
    %% gen features
    % extract segments
    dataSegments = extract_segments(data, width);
    
    [~, dataPlot] = split_data(dataSegments, nPlot, randomSeed);
    show_images(dataPlot, figureDim, [figDir, 'img_def', saveKey]);
    
    % generate bags
    dataBags = extract_instances(dataSegments, data);
    
    % unpack instances
    [instances, nInstances(count), labelInstances] = unpack_instances(dataBags);
    
    % reconstruc images
    dataImagesMeanShift = update_image(dataSegments, dataBags);
    [~, dataPlot] = split_data(dataImagesMeanShift, nPlot, randomSeed);
    show_images(dataPlot, figureDim, [figDir, 'img_seg', saveKey]);
    
    %% store data in prtools
    % Compute distances, and store in prdata set
    m = compute_distance(dataBags, instances, labelInstances, sigma);
    prData = prdataset(m',dataBags.y);
    
    prData = setprior(prData, [0.5, 0.5]);
    
    %% classify
    
    for pcaI = 1:length(pcaDims)
        textprogressbar('Classifying data: ');
        
        mapping = pcam(prData, pcaDims(pcaI)); % 
        
        for i = 1:nError
            textprogressbar(i / nError * 100);
            [dataTrain, dataTest] = gendat(prData,0.5);
            
            % if pcaI ~= length(pcaDims)
            dataTrain = prmap(dataTrain, mapping);
            dataTest = prmap(dataTest, mapping);
            % end
            
            errorRateTrain(i, pcaI) = testc(dataTrain, dataTrain * c);
            errorRateTrue(i, pcaI) = testc(dataTest, dataTrain * c);
        end
        textprogressbar('done');
        
    end
    
    errorRateTrueMean = mean(errorRateTrue, 1);
    errorRateTrueStd = std(errorRateTrue, 1);
    
    errorRateTrainMean = mean(errorRateTrain, 1);
    errorRateTrainStd = mean(errorRateTrain, 1);
    close all
end

%% plot result
figure('Position', figureDim)

errorbar(pcaDims, errorRateTrueMean, errorRateTrueStd, 'Linewidth', 2)
hold on
errorbar(pcaDims, errorRateTrainMean, errorRateTrainMean, 'Linewidth', 2)

legend('True error', 'Train error')
xlabel('pca')
ylabel('error rate')
title('Error Rate For Different PCA mappings')
ylim([0,0.08])
xlim([min(pcaDims), max(pcaDims)])
saveas(gcf, [figDir, 'error_rate_', 'pca'], 'epsc');
saveas(gcf, [figDir, 'error_rate_', 'pca'], 'fig');