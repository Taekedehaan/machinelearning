function class = combine_instance_labels(labels)
uniqueLabels = unique(labels);

for i = 1:length(uniqueLabels)
    count(i) = sum(uniqueLabels(i) == labels);
end

[~, class] = max(count);

end