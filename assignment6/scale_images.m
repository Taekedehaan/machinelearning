function data = scale_images(data, scale)
images = data.x;
dim = size(images);
objects = dim(1);

for object= 1:objects
    image = cell2mat(images(object));
    imageOut(object,1) = {imresize(image,scale)};
end

data.x = imageOut;
end