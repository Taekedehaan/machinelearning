% reset
clear all
close all
clc

%% init
figureDim = [100, 100, 1150, 400];
figureDim2 = [100, 100, 1150, 800];
figDir = [pwd, filesep, 'fig', filesep];
imageDir = 'sival_apple_banana';
nPlot = 4;
scale = 0.1;
randomSeed = 1;
nError = 10;
sigma = 10;
widths = 7; %[7, 10, 20, 30, 40, 50]; %[3, 5 
pcaDims = [10, 20, 30, 40, 50];
classifierName = 'miles';

% load images
data = read_images(imageDir);

% rescale images
data = scale_images(data, scale);

%% preporcess
[~, dataPlot] = split_data(data, nPlot, randomSeed);
show_images(dataPlot, figureDim);

%% loop over width
% init loop
count = 0;

% save memory
nInstances = nan(length(widths),1);
errorRateTrain = nan(length(widths),1);
errorRateTrueMean = nan(length(widths),1);

for width = widths
    disp(['===', num2str(width), '==='])
    saveKey = num2str(width);
    count = count + 1;
    
    %% gen features
    % extract segments
    dataSegments = extract_segments(data, width);
    
    [~, dataPlot] = split_data(dataSegments, nPlot, randomSeed);
    show_images(dataPlot, figureDim, [figDir, 'img_def', saveKey]);
    
    % generate bags
    dataBags = extract_instances(dataSegments, data);
    
    % unpack instances
    [instances, nInstances(count), labelInstances] = unpack_instances(dataBags);
    
    % reconstruc images
    dataImagesMeanShift = update_image(dataSegments, dataBags);
    [~, dataPlot] = split_data(dataImagesMeanShift, nPlot, randomSeed);
    show_images(dataPlot, figureDim, [figDir, 'img_seg', saveKey]);
    
    %% store data in prtools
    switch classifierName
        case 'naive'
            prData = bags2dataset(dataBags.x,dataBags.y);
            f = plot_3d_data(prData, figureDim2, [figDir, 'data_', saveKey]);
        case 'miles'
            % Compute distances, and store in prdata set
            m = compute_distance2(dataBags, instances, labelInstances, sigma);
            prData = prdataset(m',dataBags.y);
        case 'k'
    end
    prData = setprior(prData, [0.5, 0.5]);
    
    %% classify
    switch classifierName
        case 'naive'
            errorRateTrain(count) = get_error(prData, prData * fisherc);
            for i = 1:nError
                [dataTrain, dataTest] = gendat(prData,0.5);
                errorRateTrue(i) = get_error(dataTest, dataTrain * fisherc);
            end
            textprogressbar('done');
        
        case 'miles'
            
            for pcaI = 1:length(pcaDims)
                textprogressbar('Classifying data: ');
                
                mapping = pcam(prData, pcaDims(pcaI));
                
                for i = 1:nError
                    textprogressbar(i * 10);
                    [dataTrain, dataTest] = gendat(prData,0.5);
                    
                    dataTrain = prmap(dataTrain, mapping);
                    dataTest = prmap(dataTest, mapping);
                    
                    errorRateTrain(count) = testc(prData, prData * liknonc);
                    errorRateTrue(i, pcaI) = testc(dataTest, dataTrain * liknonc);
                end
                textprogressbar('done');

            end
        case 'knn'
          
            type = 'farRef';
            kVec = 1:6;
            Rvec = 1:2:10;
            Cvec = 1:2:10;
            nRep = 4;
            
            error = nan(length(Rvec), length(Cvec), length(kVec), nRep);
            
            for k = kVec
                for iR = 1:length(Rvec)
                    for iC = 1:length(Cvec)
                        for n = 1:nRep
                            [dataTrain, dataTest] = split_data(dataBags, 0.5);
                            error(iR,iC,k, n) = cite_knn(dataTrain, dataTest, Rvec(iR), Cvec(iC), k, type);
                        end
                    end
                end
            end
            
            errorAvg = mean(error, 4);
            errorSTD = std(error, [], 4);
            
            for k = kVec
                square_plot(errorAvg, errorSTD, Rvec, Cvec, k);
                saveas(gcf, ['fig', filesep, 'knn-k',num2str(k), 'w', num2str(width)], 'jpg')
                saveas(gcf, ['fig', filesep, 'knn-k',num2str(k), 'w', num2str(width)], 'epsc')
            end

    end
    
    errorRateTrueMean(count) = mean(errorRateTrue, 1);
    
    disp(['Number of instances: ', num2str(nInstances(count))]);
    disp(['Train error rate: ', num2str(errorRateTrain(count))]);
    disp(['True error rate: ', num2str(errorRateTrueMean(count))]);
    
    close all
end

%% plot result
figure('Position', figureDim)
hold on

plot(widths, errorRateTrueMean)
plot(widths, errorRateTrain)

legend('True error', 'Train error')
xlabel('width')
ylabel('error rate')
title('Error Rate For Different Widths')
ylim([0,0.5])
saveas(gcf, [figDir, 'error_rate_', classifierName], 'epsc');
saveas(gcf, [figDir, 'error_rate_', classifierName], 'fig');