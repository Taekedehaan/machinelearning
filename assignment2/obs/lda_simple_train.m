function [w, x0, meanC, varC, priorC] = lda_simple_train(data,y, lab)
%UNTITLED7 Summary of this function goes here
%   only works for two classes

data1C = data(y == lab(1),:);
data2C = data(y == lab(2),:);
data0C = data(y == lab(end),:);

% Train LDA Classifier
meanC1 = mean(data1C,1);
meanC2 = mean(data2C,1);

varC1 = mean(var(data1C,1));
varC2 = mean(var(data2C,1));

meanC = [meanC1; meanC2];

varC = mean([varC1; varC2]);

priorC(1) = sum(y == lab(1));
priorC(2) = sum(y == lab(2));


w = meanC(1,:) - meanC(2,:);
x0 = 1/2 * (meanC(1,:) + meanC(2,:)) - varC * log(priorC(1)/priorC(2)) * (meanC(1,:) - meanC(2,:)) ./sum((meanC(1,:) - meanC(2,:)).^2);

end

