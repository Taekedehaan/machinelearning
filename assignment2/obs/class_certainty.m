function [classifiedPercentage] = class_certainty(samples, yIn, label)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
nClusterCurrent = size(samples,2);
classifiedPercentage = nan(2, nClusterCurrent);

for i = 1:nClusterCurrent   
    sampleIndex = samples(~isnan(samples(:,i)),i);
    
    % Determine fractions
    classifiedPercentage(1,i) = sum(yIn(sampleIndex) == label(1))/sum(yIn(sampleIndex) ~= label(3));
    classifiedPercentage(2,i) = sum(yIn(sampleIndex) == label(2))/sum(yIn(sampleIndex) ~= label(3));
end
end

