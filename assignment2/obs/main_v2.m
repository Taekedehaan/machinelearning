clear all
close all
clc

%% init
% parameters
c = 2;
n.total = 10 * [4, 4];
dataSamples = sum(n.total);
n.labled = 4 * [1, 1];
n.unlabled = [n.total(1) - n.labled(1), n.total(2) - n.labled(2)];
lab = [1, 2];

meanData = [5, 5; -5, -5];
varData = [5, 5; 5, 5];

% plot layout
lineWidth = 3; %line width, used for graphs
fontSize = 14; %font size, used for graphs
colorBlue = [0, 0.4470, 0.7410]; % blue, used for plotting
colorGrey = 0.7*[1, 1, 1]; % grey, used for plotting
dim = [200, -200, 600, 600];
dim1 = [200, 200, 600, 600];
dimWide = [200, -200, 1200, 600];

% gen. sample labels
yLab = [repmat(lab(1), n.total(1), 1); 
    repmat(lab(2), n.total(2), 1)];

yUnLab = [repmat(lab(1), n.labled(1), 1); zeros(n.unlabled(1), 1); 
    repmat(lab(2), n.labled(2), 1); zeros(n.unlabled(2), 1)];

% generate mean and variance vector
meanData1 = repmat(meanData(1,:),n.total(1),1);
meanData2 = repmat(meanData(2,:),n.total(2),1);
varData1 = repmat(varData(1,:),n.total(1),1);
varData2 = repmat(varData(2,:),n.total(2),1);

% gendata
data1 = normrnd(meanData1,varData1);
data2 = normrnd(meanData2,varData2);

data = [data1; data2];

%% Label
% Label Data
data1Unlab = data(yUnLab == lab(1),:);
data2Unlab = data(yUnLab == lab(2),:);
data0Unlab = data(yUnLab == 0,:);

%% plot data
figure('Position',dim1)
plot(data1Unlab(:,1), data1Unlab(:,2),'o','LineWidth', lineWidth);
hold on
plot(data2Unlab(:,1), data2Unlab(:,2),'o','LineWidth', lineWidth);
hold on
plot(data0Unlab(:,1), data0Unlab(:,2),'o','Color', colorGrey,'LineWidth', lineWidth);
hold on
xlabel('Feature 1')
ylabel('Feature 2')
title('Classifier 1')
grid minor
axis([min(data(:,1)), max(data(:,1)), min(data(:,2)), max(data(:,2))])
set(gca,'Fontsize',fontSize)

samples = nan(dataSamples,dataSamples);
samples(1,:) = 1:dataSamples;
nSamples = ones(1,dataSamples);
nClusterCurrent = size(samples,2);
% compute distances
distance = (data(:,1) - data(:,1)').^2 + (data(:,2) - data(:,2)').^2;
distance(distance == 0) = nan;
counter = 0;

determine(:,:,1) = repmat(1:dataSamples,dataSamples,1);
determine(:,:,2) = repmat(1:dataSamples,dataSamples,1)';

% Stop at 2 clusters
while(nClusterCurrent > 2)
    distance(logical(eye(size(distance,2)))) = nan;
    % increment counter
    counter = counter + 1;
    disp(counter);
    
    % find clusters to merge
    valueMin(counter) = min(min(distance));
    [clusterMin, temp] = find(valueMin(counter) == distance);
    clusterMin = sort(clusterMin);
    
    % fid corrseponding samples
    sampleMin = samples(:, clusterMin);
    sampleMin(isnan(sampleMin)) = [];
    
    index1 = determine(clusterMin(1),clusterMin(2),1);
    index2 = determine(clusterMin(1),clusterMin(2),2);
    plot([data(index1,1), data(index2,1)], [data(index1,2), data(index2,2)],'LineWidth', lineWidth, 'Color', colorBlue);
    hold on
    
    
    %% Update distance
    % find new distance  
    newCluster = data(sampleMin,:);
    
    % reset dist
    newDist = nan(nClusterCurrent,1);
    newDetermine1 = nan(nClusterCurrent,1);
    newDetermine2 = nan(nClusterCurrent,1);
    
    % loop over all data clusters
    for i = 1:nClusterCurrent       
        % found the corresponding sample to the data cluster
        samplesCurrent = samples(1:nSamples(i),i);  
        
        % determine distance between to clusters
        distTemp = distance(i, clusterMin);
            
        %minimum distance between other sample and cluset
        [distTempMin, iSampleClusterMin] = min(distTemp,[],2);
        
        % minimum distance between two clusters
        [newDist(i), iSampleOtherTemp] = min(distTempMin);
        
        % Determine which samples determine distance
        newDetermine1(i) = sampleMin(iSampleClusterMin(iSampleOtherTemp));
        newDetermine2(i) = samplesCurrent(iSampleOtherTemp);
    end

    nSamples(clusterMin(2)) = [];
    
    indexMove = samples(:,clusterMin(2));
    indexMove(isnan(indexMove)) = [];
    iMoveRow = (nSamples(clusterMin(1)) + 1) : (nSamples(clusterMin(1)) + length(indexMove));
    iMoveCol = clusterMin(1);
    
    %update index
    samples(iMoveRow, iMoveCol) =  indexMove;
    samples(:, clusterMin(2)) = [];
    nSamples(clusterMin(1)) = sum(~isnan(samples(:, clusterMin(1))));
    
    % update distances
    distance(:,clusterMin(1)) = newDist;
    distance(clusterMin(1),:) = newDist;
    distance(:,clusterMin(2)) = [];
    distance(clusterMin(2),:) = [];
    
    % update determine
    determine(:,clusterMin(1),1) = newDetermine1;
    determine(clusterMin(1),:,1) = newDetermine1;
    determine(:,clusterMin(1),2) = newDetermine2;
    determine(clusterMin(1),:,2) = newDetermine2;
    determine(:,clusterMin(2),:) = [];
    determine(clusterMin(2),:,:) = [];
    
    % put distance to self to zero
    distance(distance == 0) = nan;
    
    nClusterCurrent  = size(samples,2);
    nCluster(counter) = nClusterCurrent;
end

figure('Position',dim)
plot(nCluster, valueMin)
xlabel('Number of Clusters')
ylabel('Disimilarity')
title('Disimilarity')
grid minor
set(gca,'Fontsize',fontSize)


% split indexed data into 2
index1 = samples(:,1);
index1(isnan(index1)) = [];

index2 = samples(:,2);
index2(isnan(index2)) = [];

% Determine fractions
c1p1 = sum(yUnLab(index1) == 1)/n.labled(1);
c1p2 = sum(yUnLab(index1) == 2)/n.labled(2);
c2p1 = sum(yUnLab(index2) == 1)/n.labled(1);
c2p2 = sum(yUnLab(index2) == 2)/n.labled(2);
disp('For cluster 1:')
disp(['Likelyness Class 1: ', num2str(c1p1)]);
disp(['Likeliness Class 2: ', num2str(c1p2)]);

disp('For cluster 2:')
disp(['Likelyness Class 1: ', num2str(c2p1)]);
disp(['Likeliness Class 2: ', num2str(c2p2)]);

yC = zeros(dataSamples,1);
if c1p1 > c1p2
    yC(index1) = lab(1);
else
    yC(index1) = lab(2);
end

if c2p1 > c2p2
    yC(index2) = lab(1);
else
    yC(index2) = lab(2);
end

% Label Data
data1C = data(yC == lab(1),:);
data2C = data(yC == lab(2),:);
data0C = data(yC == 0,:);

%% Compare results
figure('Position',dimWide)
subplot(1,2,1)

plot(data1(:,1), data1(:,2),'o','LineWidth', lineWidth);
hold on
plot(data2(:,1), data2(:,2),'o','LineWidth', lineWidth);
hold on
legend('Class 1', 'Class 2')
xlabel('Feature 1')
ylabel('Feature 2')
title('Data defined as')
grid minor
axis([min(data(:,1)), max(data(:,1)), min(data(:,2)), max(data(:,2))])
set(gca,'Fontsize',fontSize)

subplot(1,2,2)

plot(data1C(:,1), data1C(:,2),'o','LineWidth', lineWidth);
hold on
plot(data2C(:,1), data2C(:,2),'o','LineWidth', lineWidth);
hold on
legend('Class 1', 'Class 2')
xlabel('Feature 1')
ylabel('Feature 2')
title('Data combined as')
grid minor
axis([min(data(:,1)), max(data(:,1)), min(data(:,2)), max(data(:,2))])
set(gca,'Fontsize',fontSize)

disp(['Percentage correct: ', num2str(sum(yLab == yC)/dataSamples)]);

%% other stuff




figure('Position',dim)
plot(data1C(:,1), data1C(:,2),'o');
hold on
plot(data2C(:,1), data2C(:,2),'o');
hold on
legend('Class 1', 'Class 2')
xlabel('Feature 1')
ylabel('Feature 2')
title('Classifier 1')
grid minor
axis([min(data(:,1)), max(data(:,1)), min(data(:,2)), max(data(:,2))])
set(gca,'Fontsize',fontSize)

% Train LDA Classifier
meanC1 = mean(data1C,1);
meanC2 = mean(data2C,1);

varC1 = mean(var(data1C,1));
varC2 = mean(var(data2C,1));

varC = mean([varC1, varC2]);
priorC1 = sum(yUnLab == lab(1));
priorC2 = sum(yUnLab == lab(2));

w = meanC1 - meanC2;
x0 = 1/2 * (meanC1 + meanC2) - varC * log(priorC1/priorC2) * (meanC1 - meanC2) ./sum((meanC1 - meanC2).^2);

x(1,:) = min(data(:,1)) : 0.01 : max(data(:,1));
x(2,:) = x0(2) - w(1)/w(2) .* (x(1,:) - x0(1));

plot(x(1,:), x(2,:),'Linewidth', lineWidth);
%hold on
%
%dist1 = sum((data(iMin(1),:) - data(:,:)).^2,2);
%dist2 = sum((data(iMin(2),:) - data(:,:)).^2,2);
%dist = min(dist1, dist2);