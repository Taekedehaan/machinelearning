clear all
close all
clc

d = 10;
classifiedAmountThreshold = 0.6;
classifiedCertaintyThreshold = 0.05;
vis = 1;
complete = 1;
waitTime = 0;
nRep = 10;

lineWidth = 2;
fontSize = 15;

saveLabel = ['_', num2str(complete), '_', num2str(classifiedAmountThreshold)];

%% Init data
currentPath = pwd;

% load data 
fileName = [filesep 'data' filesep, 'magic04.txt'];
fileID = fopen([currentPath, fileName]);

% read trext file and convert to double
A = textscan(fileID,[repmat(['%f' '%c' ],1,d),'%c']); 
data.y = cell2mat(A(end));
data.x = double(cell2mat(A(1:2:(end-1))));
clear A

%%
yTemp(data.y == 'h') = 1;
yTemp(data.y == 'g') = 2;
data.y = yTemp';
%% gen labels
data.lab = unique(data.y);
data.lab = data.lab';

n.unlab = [0, 10, 20, 40, 80, 160, 320, 640]; %640; % ]
n.lab = 25;

%% Prep. data
% normalize
data.x = data.x ./ sqrt(var(data.x,1));

for i = 1:length(n.unlab)
    for ii = 1:nRep
        n.tot = n.unlab(i) + n.lab;

        % slpit data
        [dataTest, dataTrain] = split_data(data, n.tot);

        % Clear labels
        dataOriginal = clear_label(dataTrain, n.unlab(i));

        %% Cluster data (lda)
        [w, x0, ~,~, ~] = lda_train(dataOriginal);

        yUnclassified = dataOriginal.y == dataOriginal.lab(3);

        % classify unlabled data
        dataCluster = lda_classify(w, x0, dataOriginal, yUnclassified);

        %% compute errors
        [wOriginal, x0Original, ~,~, ~] = lda_train(dataCluster);
        [wCluster, x0Cluster, ~,~, ~] = lda_train(dataOriginal);

        errorAppTrain(i,ii) = lda_test(wCluster, x0Cluster, dataTrain);
        errorAppCluster(i,ii) = lda_test(wOriginal, x0Original, dataTrain);

        errorTrueTrain(i,ii) = lda_test(wCluster, x0Cluster, dataTest);
        errorTrueCluster(i,ii) = lda_test(wOriginal, x0Original, dataTest);

    end
    
    if vis
        % disp(['Labels added: ', num2str(sum(iClustered) - n.lab)]);
        % disp(['Percentage labled: ', num2str(sum(iClustered)/length(dataCluster.y))]);

        % disp(['Clustering error: ', num2str(errorCluster)]);

        disp(['Apparent error original data: ', num2str(mean(errorAppTrain(i,ii),2))]);
        disp(['Apparent error clustered data: ', num2str(mean(errorAppCluster(i,ii),2))]);

        disp(['True error original data: ', num2str(mean(errorTrueTrain(i,ii),2))]);
        disp(['Ture error clustered data: ', num2str(mean(errorTrueCluster(i,ii),2))]);
    end
end

%
% for i = 1:length(n.unlab)
%     for ii = 1:nRep
%         Amount of data
%         n.tot = n.unlab(i) + n.lab;
% 
%         slpit data
%         [dataTest, dataTrain] = split_data(data, n.tot);
% 
%         Clear labels
%         dataOriginal = clear_label(dataTrain, n.unlab(i));
% 
%         % Cluster data
%         [dataCluster, nClusterCurrent, classCertainty, nSamples] = cluster_custom(dataOriginal, complete, classifiedAmountThreshold, classifiedCertaintyThreshold, waitTime, vis); %(i)
% 
%         iClustered = dataCluster.y ~= dataCluster.lab(end);
% 
%         errorCluster(i,ii) = sum(dataTrain.y(iClustered) ~= dataCluster.y(iClustered))/length(dataTrain.y(iClustered));
% 
%         
%     end
% end

% figure
% semilogx(n.unlab, mean(errorCluster,2), 'LineWidth', lineWidth)
% saveas(gca, ['fig/clusterError', saveLabel], 'jpg')
% saveas(gca, ['fig/clusterError.eps', saveLabel], 'epsc')

figure
semilogx(n.unlab, mean(errorAppTrain,2), 'LineWidth', lineWidth)
hold on
semilogx(n.unlab, mean(errorAppCluster,2), 'LineWidth', lineWidth)
grid minor
xlabel('Unlabled data [-]')
ylabel('Error [-]')
legend('Non-clustered data', 'Clustered data')
title('Apperent Error')
set(gca,'Fontsize',fontSize)
axis([min(n.unlab), max(n.unlab), 0, max(max(errorTrueCluster))])
saveas(gca, ['fig/appError', saveLabel], 'jpg')
saveas(gca, ['fig/appError.eps', saveLabel], 'epsc')

figure
semilogx(n.unlab, mean(errorTrueTrain,2), 'LineWidth', lineWidth)
hold on
semilogx(n.unlab, mean(errorTrueCluster,2), 'LineWidth', lineWidth)
grid minor
xlabel('Unlabled data [-]')
ylabel('Error [-]')
legend('Non-clustered data', 'Clustered data')
title('True Error')
set(gca,'Fontsize',fontSize)
axis([min(n.unlab), max(n.unlab), 0, max(max(errorTrueCluster))])
saveas(gca, ['fig/trueError', saveLabel], 'jpg')
saveas(gca, ['fig/trueError', saveLabel, '.eps'], 'epsc')