clear all
close all
clc

%% init
% Cluster parameters
complete = true;
classifiedThreshold = 0.6;
classifiedCertaintyThreshold = 0.05;
waitTime = 0;
vis = true;

% data
n.total =  [1000, 1000];
dataSamples = sum(n.total);
lab = [1,2];

n.lab = 10;
n.unlab = 100;
n.train = n.lab + n.unlab;

meanData = 1.2* [5, 5; -5, -5];
varData = [5, 5; 5, 5];

% plot layout
lineWidth = 3; %line width, used for graphs
fontSize = 14; %font size, used for graphs
colorBlue = [0, 0.4470, 0.7410]; % blue, used for plotting
colorGrey = 0.7*[1, 1, 1]; % grey, used for plotting
dim = [200, -200, 600, 600];
dim1 = [200, 200, 600, 600];
dimWide = [200, -200, 1200, 600];

% generate mean and variance vector
meanData1 = repmat(meanData(1,:),n.total(1),1);
meanData2 = repmat(meanData(2,:),n.total(2),1);
varData1 = repmat(varData(1,:),n.total(1),1);
varData2 = repmat(varData(2,:),n.total(2),1);

% gendata
data1 = normrnd(meanData1,varData1);
data2 = normrnd(meanData2,varData2);
dataOriginal.x = [data1; data2];

% gen. sample labels
dataOriginal.y = [repmat(lab(1), n.total(1), 1); 
    repmat(lab(2), n.total(2), 1)];

dataOriginal = generate_labels(dataOriginal);

%% split
[dataTest, dataTrain] = split_data(dataOriginal, n.train);

% we don't know all labels
dataMissLab = clear_label(dataTrain,n.unlab);

%% Label
[dataCluster, nCluster] = cluster_custom(dataMissLab, complete, classifiedThreshold, classifiedCertaintyThreshold, waitTime, vis);
disp(['Number of clusters: ' num2str(nCluster)]);

% cluster error
iClustered = dataCluster.y ~= dataCluster.lab(end);
errorCluster = sum(dataTrain.y(iClustered) ~= dataCluster.y(iClustered))/length(dataTrain.y(iClustered));
disp(['Percentage incorrectly clustered: ', num2str(errorCluster)]);

%% Train LDA
[wMissLab, x0MissLab, ~,~, ~] = lda_train(dataMissLab);
[wCluster, x0Cluster, ~,~, ~] = lda_train(dataCluster);

% compute errors
errorAppMissLab = lda_test(wMissLab, x0MissLab, dataMissLab);
disp(['Apparent error original data: ', num2str(errorAppMissLab)]);

errorAppCluster = lda_test(wCluster, x0Cluster, dataCluster);
disp(['Apparent error clustered data: ', num2str(errorAppCluster)]);

errorTrueMissLab = lda_test(wMissLab, x0MissLab, dataTest);
disp(['True error original data: ', num2str(errorTrueMissLab)]);

errorTrueCluster = lda_test(wCluster, x0Cluster, dataTest);
disp(['Ture error clustered data: ', num2str(errorTrueCluster)]);

%% plot data
plot(dataCluster.x(dataCluster.y == 0,1), dataCluster.x(dataCluster.y == 0,2),'o','Color', colorGrey,'LineWidth', lineWidth);
hold on
figure('Position',dim1)
plot_data(dataCluster)
xlabel('Feature 1')
ylabel('Feature 2')
title('Cluster result')
grid minor
axis([min(dataCluster.x(:,1)), max(dataCluster.x(:,1)), min(dataCluster.x(:,2)), max(dataCluster.x(:,2))])
set(gca,'Fontsize',fontSize)
saveas(gca, 'fig/dataIn', 'jpg')
saveas(gca, 'fig/dataIn.eps', 'epsc')

%% Comparison figure
figure('Position',dimWide)

%original data
subplot(1,3,1)
plot_data(dataTrain)

legend('Class 1', 'Class 2')
xlabel('Feature 1')
ylabel('Feature 2')
title('Original data')
grid minor
axis([min(dataTrain.x(:,1)), max(dataTrain.x(:,1)), min(dataTrain.x(:,2)), max(dataTrain.x(:,2))])
set(gca,'Fontsize',fontSize)

% unlabled
subplot(1,3,2)
plot_data(dataMissLab)

legend('Class 1', 'Class 2')
xlabel('Feature 1')
ylabel('Feature 2')
title('Original data')
grid minor
axis([min(dataTrain.x(:,1)), max(dataTrain.x(:,1)), min(dataTrain.x(:,2)), max(dataTrain.x(:,2))])
set(gca,'Fontsize',fontSize)

% Clustered data
subplot(1,3,3)
plot_data(dataCluster)
legend('Class 1', 'Class 2')
xlabel('Feature 1')
ylabel('Feature 2')
title('Clustered data')
grid minor
axis([min(dataCluster.x(:,1)), max(dataCluster.x(:,1)), min(dataCluster.x(:,2)), max(dataCluster.x(:,2))])
set(gca,'Fontsize',fontSize)

%% Plot LDA
xCluster = min(dataCluster.x(:,1)) : 0.01 : max(dataCluster.x(:,1));
xCluster = lda_plot(wCluster, x0Cluster, xCluster);

xMissLab = min(dataMissLab.x(:,1)) : 0.01 : max(dataMissLab.x(:,1));
xMissLab = lda_plot(wMissLab, x0MissLab, xMissLab);

figure('Position',dim)
subplot(1,2,1)
plot_data(dataMissLab)
hold on
hold on
plot(xMissLab(1,:), xMissLab(2,:),'Linewidth', lineWidth);
legend('Class 1', 'Class 2')
xlabel('Feature 1')
ylabel('Feature 2')
title('Missing Labels')
grid minor
set(gca,'Fontsize',fontSize)

% cluster
subplot(1,2,2)
plot_data(dataCluster)
hold on
hold on
plot(xCluster(1,:), xCluster(2,:),'Linewidth', lineWidth);
legend('Class 1', 'Class 2')
xlabel('Feature 1')
ylabel('Feature 2')
title('Clustering 1')
grid minor
set(gca,'Fontsize',fontSize)
