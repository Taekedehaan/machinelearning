function plot_data(data, classifier)
lineWidth = 2;
colorGrey = 0.7*[1, 1, 1]; % grey, used for plotting
data.lab(3) = 0;

plot(data.x(data.y == data.lab(1),1), data.x(data.y == data.lab(1),2),'o','LineWidth', lineWidth);
hold on
plot(data.x(data.y == data.lab(2),1), data.x(data.y == data.lab(2),2),'o','LineWidth', lineWidth);
hold on
if any(data.y == data.lab(3))
    h0 = plot(data.x(data.y == data.lab(3),1), data.x(data.y == data.lab(3),2),'o','LineWidth', lineWidth, 'Color', colorGrey);
    hold on
end

% plot classifier
if classifier
    plot(classifier(1,:), classifier(2,:),'Linewidth', lineWidth);
end

% set axis
axis([min(data.x(:,1)), max(data.x(:,1)), min(data.x(:,2)), max(data.x(:,2))])

% add legend
if any(data.y == data.lab(3))
    legend('Class 1', 'Class 2', 'Unkown','LDA','Location','northwest')
    uistack(h0,'bottom')
else
    legend('Class 1', 'Class 2','LDA','Location','northwest')
end

% label
xlabel('Feature 1')
ylabel('Feature 2')

end

