function [data] = load_magic_data()
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
d = 10; % dimension

currentPath = pwd;

% load data 
fileName = [filesep 'data' filesep, 'magic04.txt'];
fileID = fopen([currentPath, fileName]);

% read trext file and convert to double
A = textscan(fileID,[repmat(['%f' '%c' ],1,d),'%c']); 
data.y = cell2mat(A(end));
data.x = double(cell2mat(A(1:2:(end-1))));
clear A

% set labels
yTemp(data.y == 'h') = 1;
yTemp(data.y == 'g') = 2;
data.y = yTemp';

% gen labels
data = generate_labels(data);

n.total = [sum(data.y == data.lab(1)), sum(data.y == data.lab(2))];

data.prior(1) = sum(data.y == data.lab(1))/sum(n.total);
data.prior(2) = sum(data.y == data.lab(2))/sum(n.total);
end

