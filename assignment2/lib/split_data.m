function [dataTest, dataTrain] = split_data(data, pTrain)
    % for data use data.x and data.y where:
    %   - x: feture information
    %   - y: labels
    % if ptrain < 1 it is seen as a fraction of total data
    % if ptrain > 1 it is seen as number of data samples
    

    % data size
    nData = size(data.x,1);
    iData = size(data.x,2);
    
    % size of training pool
    if pTrain >= 1
        nTrain = pTrain;
    else
        nTrain = round(pTrain * nData);
    end
    
    %create randowm vectors
    iRandom = randperm(nData);
    
    if nTrain == 1
        iTrain1 = iRandom(find(data(iRandom,end) == 1,1));
        iTrain2 = iRandom(find(data(iRandom,end) == 2,1));
        
        iTest = iRandom;
        iTrain = [iTrain1,iTrain2];
        
        for i = 1:length(iTrain)
            iTest(iTest == iTrain(i)) = [];
        end
    else
        % simply pick random samples from data
        % May result in uneven classes!
        iTrain = iRandom(1:nTrain);
        iTest = iRandom((nTrain+1):nData);
    end
    
    % selec tdata
    dataTest.x = data.x(iTest,:);
    dataTest.y = data.y(iTest,:);
    dataTest.lab = data.lab;
    dataTest.prior = data.prior;
    
    dataTrain.x = data.x(iTrain,:);
    dataTrain.y = data.y(iTrain,:);
    dataTrain.lab = data.lab;
    dataTrain.prior = data.prior;
    
    if length(iTest) + length(iTrain) ~= nData
        warning('problem with test and train data size');
    end
    
    dataTrain = update_prior(dataTrain);
    dataTest = update_prior(dataTest);
end

