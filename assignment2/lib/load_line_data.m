function [data] = load_line_data()
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
% generate mean and variance vector
n.total =  [1000, 1000];
lab = [1,2];

n.lab = 10;
n.unlab = 100;
n.train = n.lab + n.unlab;

meanData = 1.2* [0, 0 ; 0, 0];
varData = 1 * [0.1, 0.1; 0.1, 0.1];

meanData1 = repmat(meanData(1,:),n.total(1),1);
meanData2 = repmat(meanData(2,:),n.total(2),1);
varData1 = repmat(varData(1,:),n.total(1),1);
varData2 = repmat(varData(2,:),n.total(2),1);

x1 = linspace(-1,1,n.total(1))';
x2 = x1 + normrnd(meanData1(:,2),varData1(:,2));
data1 = [x1,x2];

x1 = linspace(-1,1,n.total(2))';
x2 = 1 + x1 + normrnd(meanData2(:,2),varData2(:,2));
data2 = [x1,x2];

data.x = [data1; data2];

% gen. sample labels
data.y = [repmat(lab(1), n.total(1), 1); 
    repmat(lab(2), n.total(2), 1)];

data = generate_labels(data);

data = update_prior(data);

end
