function [data] = clear_label_normal(data)
%Clears label of nNoLab samples
%   Detailed explanation goes here
    if ischar(data.y)
        emptyLab = '0';
    elseif isnumeric(data.y)
        emptyLab = 0;
    end

    nTot = length(data.y);

    iKeep1 = data.x(:,1) > 3 & data.y == data.lab(1);
    iKeep2 = data.x(:,1) < 0 & data.y == data.lab(2);
    
    iClear = ~(iKeep1 | iKeep2);
    data.y(iClear) = emptyLab;
    data = generate_labels(data);
end

