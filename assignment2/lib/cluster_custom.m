function [data, nClusterCurrent, classCertainty, nSamples] = cluster_custom(data, complete, classifiedThreshold, classifiedCertaintyThreshold, t, vis)
%clustering alogrithm using single or complete linkage
%   Detailed explanation goes here
x = data.x;
y = data.y;
lab = data.lab;
nClusterDesired = 20;
n = size(x,1);

%% plot layout
lineWidth = 3; %line width, used for graphs
fontSize = 14; %font size, used for graphs
colorBlue = [0, 0.4470, 0.7410]; % blue, used for plotting
colorGrey = 0.7*[1, 1, 1]; % grey, used for plotting
dim = [200, -200, 600, 600];
dim1 = [200, 200, 600, 600];
dimWide = [200, -200, 1200, 600];

%% init memory
samples = nan(n,n); % contains samples per cluster
samples(1,:) = 1:n;
nSamples = ones(1,n);
nClusterCurrent = size(samples,2);
nCluster = nan(n - nClusterDesired, 1);
valueMin = nan(n - nClusterDesired, 1);

%% init plot
if vis
    figure('Position',dim1)
    plot(x(y == lab(3),1), x(y == lab(3),2),'o','Color', colorGrey,'LineWidth', lineWidth);
    hold on
    plot(x(y == lab(1),1), x(y == lab(1),2),'o','LineWidth', lineWidth);
    hold on
    plot(x(y == lab(2),1), x(y == lab(2),2),'o','LineWidth', lineWidth);
    hold on
    xlabel('Feature 1')
    ylabel('Feature 2')
    title('Classifier 1')
    grid minor
    axis([min(x(:,1)), max(x(:,1)), min(x(:,2)), max(x(:,2))])
    set(gca,'Fontsize',fontSize)
end
distance = zeros(n,n);

%% compute distances
distance = dist(data.x');
%for i = 1:size(x,2)
%    distance = distance + (x(:,i) - x(:,i)').^2;
%end

%distance = sqrt(distance);

distance(distance == 0) = nan;
counter = 0;

determine(:,:,1) = repmat(1:n,n,1);
determine(:,:,2) = repmat(1:n,n,1)';

%% Cluster until desired amount of clusters remain
while true % (nClusterCurrent > nClusterDesired) %
    
    %% Pause maybe for visualization
    if t
        pause(t);
    end
    
    %% Stop condition
    % check if all clusters have a label
    classCertainty = class_certainty(samples, y, lab);
    iUnDetermined = abs(classCertainty(1,:) - classCertainty(2,:)) <= classifiedCertaintyThreshold;
    
    % determine fraction classified
    pClassified = (~iUnDetermined * nSamples')/n;  
    if pClassified >= classifiedThreshold
        break;
    end
    
    if nClusterCurrent == 2
        break;
    end
    
    %% Init new loop
    %make sure distance to self is nan
    distance(logical(eye(size(distance,2)))) = nan;
    
    % increment counter
    counter = counter + 1;
    %if counter == 77 for debugging purposes
    %    disp('Stop!');
    %end
    
    if vis
        if ~rem(counter,20)
            disp(counter/(n - nClusterDesired));
        end
    end
    %% Determine what to merge
    % find clusters to merge
    valueMin(counter) = min(min(distance));
    [clusterMinRow, clusterMinCol] = find(valueMin(counter) == distance);

    clusterMin = [clusterMinRow(1), clusterMinCol(1)]';
    clusterMin = sort(clusterMin);
    
    if vis
        %show which samples are causeing the new merge
        index1 = determine(clusterMin(1),clusterMin(2),1);
        index2 = determine(clusterMin(1),clusterMin(2),2);

        plot([x(index1,1), x(index2,1)], [x(index1,2), x(index2,2)],'LineWidth', lineWidth, 'Color', colorBlue);
        hold on
    end
 
    %% Update distance
    % determine distance between to clusters
    distTemp = distance(1:nClusterCurrent, clusterMin);

    %minimum distance between other sample and cluset
    if complete
        [newDist, iSampleClusterMin] = max(distTemp,[],2);
    else
        [newDist, iSampleClusterMin] = min(distTemp,[],2);
    end
    
    % Determine which samples determine distance
    row = (1:nClusterCurrent)';
    col = clusterMin(iSampleClusterMin);
    loc1 = sub2ind(size(determine), row, col, 1 * ones(nClusterCurrent,1));
    loc2 = sub2ind(size(determine), row, col, 2 * ones(nClusterCurrent,1));
    newDetermine1 = determine(loc1);
    newDetermine2 = determine(loc2);
    
    nSamples(clusterMin(2)) = [];
    
    indexMove = samples(:,clusterMin(2));
    indexMove(isnan(indexMove)) = [];
    iMoveRow = (nSamples(clusterMin(1)) + 1) : (nSamples(clusterMin(1)) + length(indexMove));
    iMoveCol = clusterMin(1);
    
    %update index
    samples(iMoveRow, iMoveCol) =  indexMove;
    samples(:, clusterMin(2)) = [];
    nSamples(clusterMin(1)) = sum(~isnan(samples(:, clusterMin(1))));
    
    if sum(nSamples) ~= n
        warning(['We are losing samples on itteration: ', num2str(counter), ', with complete linkage: ' num2str(complete)]);
    end
    
    % update distances
    distance(:,clusterMin(1)) = newDist;
    distance(clusterMin(1),:) = newDist;
    distance(:,clusterMin(2)) = [];
    distance(clusterMin(2),:) = [];
    
    % update determine
    determine(:,clusterMin(1),1) = newDetermine1;
    determine(clusterMin(1),:,1) = newDetermine1;
    determine(:,clusterMin(1),2) = newDetermine2;
    determine(clusterMin(1),:,2) = newDetermine2;
    determine(:,clusterMin(2),:) = [];
    determine(clusterMin(2),:,:) = [];
    
    nClusterCurrent  = size(samples,2);
    nCluster(counter) = nClusterCurrent;    
end

%%
if vis
    figure('Position',dim)
    plot(nCluster, valueMin)
    xlabel('Number of Clusters')
    ylabel('Disimilarity')
    title('Disimilarity')
    grid minor
    set(gca,'Fontsize',fontSize)
    saveas(gca, 'fig/cluster', 'jpg')
    saveas(gca, 'fig/cluster.eps', 'epsc')
end

classCertainty = class_certainty(samples, y, lab);

if vis
    for i = 1:nClusterCurrent
        disp(['For cluster', num2str(i),': '])
        disp(['Likelyness Class 1: ', num2str(classCertainty(1,i))]);
        disp(['Likeliness Class 2: ', num2str(classCertainty(2,i))]);
    end
end

%% Classify data
[~, indexMax] = max(classCertainty, [], 1);

% also unkown if both class certainties are equal (0.5)
indexMax(abs(classCertainty(1,:) - classCertainty(2,:)) <= classifiedCertaintyThreshold) = 3;
%indexMax(abs(classCertainty(1,:) - classCertainty(2,:)) <= 0.05) = 3;
yCluster(:) = lab(indexMax);

iClass1 = samples(:,yCluster == lab(1));
iClass1 = iClass1(:);
iClass1 = iClass1(~isnan(iClass1(:)));

iClass2 = samples(:,yCluster == lab(2));
iClass2 = iClass2(:);
iClass2 = iClass2(~isnan(iClass2(:)));

iClass0 = samples(:,yCluster == lab(3));
iClass0 = iClass0(:);
iClass0 = iClass0(~isnan(iClass0(:)));

yOut = nan(n,1);
yOut(iClass1) = lab(1);
yOut(iClass2) = lab(2);
yOut(iClass0) = lab(3);

data.y = yOut;

data = update_prior(data);
end

% labledCluster = 0;
%     for i = 1:nClusterCurrent 
%         sampleIndex = samples(~isnan(samples(:,i)),i);
%         if any(yIn(sampleIndex) ~= 0)
%             labledCluster = labledCluster + 1;
%         end
%     end
%     if labledCluster == nClusterCurrent
%         break;
%     end
