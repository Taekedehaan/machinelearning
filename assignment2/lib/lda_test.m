function error = lda_test(w, x0, data)
%classify data and verify results
%   Detailed explanation goes here

% store actual data labels
yTrue = data.y;

% determine data labels with lda
data = lda_classify(w, x0, data, false);
yDetermined = data.y;

% Only use data points which are labled
iLabeled = yTrue ~=  data.lab(end);
yTrue = yTrue(iLabeled);
yDetermined = yDetermined(iLabeled);
error = sum(yDetermined ~= yTrue)/sum(iLabeled);
end

