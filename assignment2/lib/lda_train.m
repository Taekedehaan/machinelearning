function [w, x0, meanC, covC] = lda_train(data)
%UNTITLED7 Summary of this function goes here
%   only works for two classes

data1C = data.x(data.y == data.lab(1),:);
data2C = data.x(data.y == data.lab(2),:);
data0C = data.x(data.y == data.lab(end),:);

% Train LDA Classifier
meanC1 = mean(data1C,1)';
meanC2 = mean(data2C,1)';


meanC = [meanC1, meanC2];

covC = cov([data1C; data2C]);

meanDiff = meanC(:,1) - meanC(:,2);

w = covC \ meanDiff;
x0 = 1/2 * (meanC(:,1) + meanC(:,2)) - log(data.prior(1)/data.prior(2)) * meanDiff /(meanDiff' / covC * meanDiff);

end

