function [data] = clear_label(data,nNoLab)
%Clears label of nNoLab samples
%   Detailed explanation goes here
    if ischar(data.y)
        emptyLab = '0';
    elseif isnumeric(data.y)
        emptyLab = 0;
    end

    nTot = length(data.y);


    iRand = randperm(nTot);
    iClear = iRand(1:nNoLab);
    data.y(iClear) = emptyLab;
    data = generate_labels(data);
end

