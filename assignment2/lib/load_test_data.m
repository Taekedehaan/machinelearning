function data = load_test_data(cMean, cVar)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
% generate mean and variance vector
n.total =  [1000, 1000];
lab = [1,2];

n.lab = 10;
n.unlab = 100;
n.train = n.lab + n.unlab;

meanData = [cMean * 1, cMean * 1; 1, 1];
varData = [cVar, cVar; 1, 1];

meanData1 = repmat(meanData(1,:),n.total(1),1);
meanData2 = repmat(meanData(2,:),n.total(2),1);
varData1 = repmat(varData(1,:),n.total(1),1);
varData2 = repmat(varData(2,:),n.total(2),1);

% gendata
data1 = [random('beta', ones(n.total(1)), 1), random('beta', ones(n.total(1)), 1)];
data2 = [1 + random('beta', ones(n.total(1)), 1), random('beta', ones(n.total(1)), 1)];
data.x = [data1; data2];

% gen. sample labels
data.y = [repmat(lab(1), n.total(1), 1); 
    repmat(lab(2), n.total(2), 1)];

data = generate_labels(data);

data.prior(1) = sum(data.y == data.lab(1))/sum(n.total);
data.prior(2) = sum(data.y == data.lab(2))/sum(n.total);
end

