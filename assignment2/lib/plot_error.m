function [fill1, fill2] = plot_error(error1,error2, n)
colorGrey = 0.7*[1, 1, 1]; % grey, used for plotting
lineWidth = 2;



semilogx(n, mean(error1,2), 'LineWidth', lineWidth)
hold on
semilogx(n, mean(error2,2), 'LineWidth', lineWidth)
hold on

top = mean(error1,2) + std(error1,0,2);
bot = mean(error1,2) - std(error1,0,2);
n2 = [n, fliplr(n)];
inBetween = [bot', fliplr(top')];
fill1 = fill(n2, inBetween, 1.2* colorGrey,'EdgeColor','none');
hold on

top = mean(error2,2) + std(error2,0,2);
bot = mean(error2,2) - std(error2,0,2);
n2 = [n, fliplr(n)];
inBetween = [bot', fliplr(top')];
fill2 = fill(n2, inBetween, 1.2* colorGrey,'EdgeColor','none');

grid minor
xlabel('Unlabled data [-]')
ylabel('Error [-]')
end

