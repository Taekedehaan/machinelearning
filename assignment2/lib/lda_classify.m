function data = lda_classify(w, x0, data, index)
%classify data and verify results
%   Detailed explanation goes here

%dataSamples = size(data.x,1);

result = w' * (data.x' - x0);
yC = data.lab(1) * (result > 0) + data.lab(2) * (result < 0);

if index
    data.y(index) = yC(index)';
else
    data.y = yC';
end

end

