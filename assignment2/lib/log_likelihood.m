function ll = log_likelihood(data, meanC, varC)

if ~issymmetric(varC)
    warning('SIGMA non symmetric');
end
if diff(size(varC))
    warning('SIGMA non square');
end

[~,p] = chol(varC); %zero if the matrix is found to be positive definite
if p
     warning('SIGMA non possitive definite');
end

%Compute log likelihood
dataC1 = data.x(data.y == data.lab(1),:); %sum(data.y == data.lab(1))
dataC2 = data.x(data.y == data.lab(2),:); %sum(data.y == data.lab(2))

pC1 = mvnpdf(dataC1,meanC(:,1)',varC);
pC2 = mvnpdf(dataC2,meanC(:,2)',varC);

ll = sum(log(pC1)) + sum(log(pC2));
end