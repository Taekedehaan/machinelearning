clear all
close all
clc

%% init
classifiedThreshold = 0.85;
classifiedCertaintyThreshold = 0.05;
vis = false;
complete = 0;
waitTime = 0;
nRep = 50;
ldaCluster = false;
% plot
lineWidth = 2;
fontSize = 15;

labelKey = input('Enter the key you want to use to store the results: ');
saveLabel = ['_', labelKey, '_', num2str(classifiedThreshold * 100)];

n.unlab = 320; % , 640
n.lab = 25;
n.tot = n.unlab + n.lab;

%% Init data
% magic
data = load_magic_data();

% normal
data = load_normal_data(3.5,1);

% line
% data = load_line_data();
%% Prep. data
% normalize
data.x = data.x ./ sqrt(var(data.x,1));

% slpit data
[dataTest, dataTrain] = split_data(data, n.tot);

% Clear labels
%dataMissLab = clear_label(dataTrain, n.unlab);
dataMissLab = clear_label_normal(dataTrain);

%% Cluster data 1 (Complete linkage)
[dataCluster1, nCluster1, ~, ~] = cluster_custom(dataMissLab, 1, classifiedThreshold, classifiedCertaintyThreshold, waitTime, vis);

% cluster error
iClustered1 = dataCluster1.y ~= dataCluster1.lab(end);
errorCluster1 = sum(dataTrain.y(iClustered1) ~= dataCluster1.y(iClustered1))/length(dataTrain.y(iClustered1));

disp(['Percentage incorrectly clustered 1: ', num2str(errorCluster1)]);

%% Cluster data 2 (Signle linkage)
if ldaCluster
    [wCluster, x0Cluster, ~,~] = lda_train(dataMissLab);

    % classify unlabled data
    yUnclassified = dataMissLab.y == dataMissLab.lab(end);
    dataCluster2 = lda_classify(wCluster, x0Cluster, dataMissLab, yUnclassified);
else
    [dataCluster2, nCluster2, ~, ~] = cluster_custom(dataMissLab, 0, classifiedThreshold, classifiedCertaintyThreshold, waitTime, vis);
end

iClustered2 = dataCluster2.y ~= dataCluster2.lab(end);
errorCluster2 = sum(dataTrain.y(iClustered2) ~= dataCluster2.y(iClustered2))/length(dataTrain.y(iClustered2));
disp(['Percentage incorrectly clustered 2: ', num2str(errorCluster2)]);

%% LDA classify
[wCluster1 , x0Cluster1, meanCluster1, varCluster1] = lda_train(dataCluster1);
[wCluster2 , x0Cluster2, meanCluster2, varCluster2] = lda_train(dataCluster2);
[wMissLab, x0MissLab , meanMissLab, varMissLab] = lda_train(dataMissLab);

errorAppCluster1 = lda_test(wCluster1, x0Cluster1, dataCluster1);
errorAppCluster2 = lda_test(wCluster2, x0Cluster2, dataCluster2);
errorAppMissLab = lda_test(wMissLab, x0MissLab, dataMissLab);

errorTrueCluster1 = lda_test(wCluster1, x0Cluster1, dataTest);
errorTrueCluster2 = lda_test(wCluster2, x0Cluster2, dataTest);
errorTrueMissLab = lda_test(wMissLab, x0MissLab, dataTest);

disp(['Apparent error data with missing label: ', num2str(errorAppMissLab)]);
disp(['Apparent error clustered data 1: ', num2str(errorAppCluster1)]);
disp(['Apparent error clustered data 2: ', num2str(errorAppCluster2)]);

disp(['True error data with missing label: ', num2str(errorTrueMissLab)]);
disp(['True error clustered data 1: ', num2str(errorTrueCluster1)]);
disp(['True error clustered data 2: ', num2str(errorTrueCluster2)]);

%% LDA log likelihood
llCluster1 = log_likelihood(dataCluster1, meanCluster1, varCluster1);
llCluster2 = log_likelihood(dataCluster2, meanCluster2, varCluster2);
llMissLab = log_likelihood(dataMissLab, meanMissLab, varMissLab);

%% Plot data
figure('Position',[200,200,800,800],'Name','data')

%original data
subplot(2,2,1)
plot_data(dataTrain, false)

legend('Class 1', 'Class 2')
xlabel('Feature 1')
ylabel('Feature 2')
title('Original data')
grid minor
axis([min(dataTrain.x(:,1)), max(dataTrain.x(:,1)), min(dataTrain.x(:,2)), max(dataTrain.x(:,2))])
set(gca,'Fontsize',fontSize)

% unlabled
subplot(2,2,2)
plot_data(dataMissLab, false)
title('Missing Label data')
grid minor
axis([min(dataTrain.x(:,1)), max(dataTrain.x(:,1)), min(dataTrain.x(:,2)), max(dataTrain.x(:,2))])
set(gca,'Fontsize',fontSize)

% Clustered data
subplot(2,2,3)
plot_data(dataCluster1, false)
title('Complete Linkage')
grid minor
axis([min(dataCluster1.x(:,1)), max(dataCluster1.x(:,1)), min(dataCluster1.x(:,2)), max(dataCluster1.x(:,2))])
set(gca,'Fontsize',fontSize)

% Clustered data
if ldaCluster
    xCluster = min(dataCluster2.x(:,1)) : 0.01 : max(dataCluster2.x(:,1));
    xCluster = lda_plot(wCluster, x0Cluster, xCluster);
end
subplot(2, 2, 4)
plot_data(dataCluster2, false)
if ldaCluster
    hold on
    plot(xCluster(1,:), xCluster(2,:),'Linewidth', lineWidth);
end
title('Single Linkage')
grid minor
axis([min(dataCluster2.x(:,1)), max(dataCluster2.x(:,1)), min(dataCluster2.x(:,2)), max(dataCluster2.x(:,2))])
set(gca,'Fontsize',fontSize)

saveas(gca, ['fig/data_', saveLabel], 'jpg')
saveas(gca, ['fig/data_', saveLabel, '.eps'], 'epsc')

%% Plot LDA
xCluster1 = min(dataCluster1.x(:,1)) : 0.01 : max(dataCluster1.x(:,1));
xCluster1 = lda_plot(wCluster1, x0Cluster1, xCluster1);

xCluster2 = min(dataCluster2.x(:,1)) : 0.01 : max(dataCluster2.x(:,1));
xCluster2 = lda_plot(wCluster2, x0Cluster2, xCluster2);

xMissLab = min(dataMissLab.x(:,1)) : 0.01 : max(dataMissLab.x(:,1));
xMissLab = lda_plot(wMissLab, x0MissLab, xMissLab);

figure('Position',[200,200,1000,400],'Name','LDA')
subplot(1,3,1)
plot_data(dataMissLab, xMissLab)
title('Missing Labels')
grid minor
set(gca,'Fontsize',fontSize)

% cluster 1
subplot(1,3,2)
plot_data(dataCluster1, xCluster1)
title('Complete Linkage')
grid minor
set(gca,'Fontsize',fontSize)

% cluster 2
subplot(1,3,3)
plot_data(dataCluster2,xCluster2)
title('Single Linkage')
grid minor
set(gca,'Fontsize',fontSize)

saveas(gca, ['fig/LDA_', saveLabel], 'jpg')
saveas(gca, ['fig/LDA_', saveLabel, '.eps'], 'epsc')