clear all
close all
clc

%% init
% cluster
classifiedThreshold = 0.6;
classifiedCertaintyThreshold = 0.05;
vis = false;
complete = 1;
waitTime = 0;
nRep = 200;
lda = false;

% plot
lineWidth = 2;
fontSize = 13;
colorGrey = 0.7*[1, 1, 1]; % grey, used for plotting

% data
n.unlab = [20, 40, 80, 160, 320, 640]; % , 640  0, 10,  , 640
n.lab = 25;

dataType = input('Enter which data set would you like to load ([1] magic data, [2] normal data, [3] line data): ');
labelKey = input('Enter the key you want to use to store the results: ');
saveLabel = ['_', labelKey, '_', num2str(classifiedThreshold * 10)];

%% Init data
if dataType == 1
    % magic
    data = load_magic_data();
elseif dataType == 2
    % custom
    data = load_normal_data(1.8,1);
elseif dataType == 3
    data = load_line_data(1.8,1);
end
%% Prep. data
% normalize
data.x = data.x ./ sqrt(var(data.x,1));

%% reserve memory
errorCluster1 = nan(length(n.unlab),nRep);
errorCluster2 = nan(length(n.unlab),nRep);

errorAppCluster1 = nan(length(n.unlab),nRep);
errorAppCluster2 = nan(length(n.unlab),nRep);
errorAppMissLab  = nan(length(n.unlab),nRep);

errorTrueCluster1 = nan(length(n.unlab),nRep);
errorTrueCluster2 = nan(length(n.unlab),nRep);
errorTrueMissLab = nan(length(n.unlab),nRep);

for i = 1:length(n.unlab)
    disp('====New round====')
    disp(['Unalabled data: ' num2str(n.unlab(i))])
    for ii = 1:nRep
        if ~mod(ii,10)
            disp(num2str(ii/nRep));
        end
        
        % Amount of data
        n.tot = n.unlab(i) + n.lab;

        % slpit data
        [dataTest, dataTrain] = split_data(data, n.tot);

        % Clear labels
        dataMissLab = clear_label(dataTrain, n.unlab(i));

        %% Cluster data 1
        [dataCluster1, nClusterCurrent1, classCertainty1, nSamples1] = cluster_custom(dataMissLab, 1, classifiedThreshold, classifiedCertaintyThreshold, waitTime, vis); %(i)

        iClustered1 = dataCluster1.y ~= dataCluster1.lab(end);
        errorCluster1(i,ii) = sum(dataTrain.y(iClustered1) ~= dataCluster1.y(iClustered1))/length(dataTrain.y(iClustered1));

        %% Cluster data 2 (LDA)
        [w, x0, ~,~] = lda_train(dataMissLab);

        % classify unlabled data
        if lda
            yUnclassified = dataMissLab.y == dataMissLab.lab(end);
            dataCluster2 = lda_classify(w, x0, dataMissLab, yUnclassified);
        else
            [dataCluster2, nClusterCurrent2, classCertainty2, nSamples2] = cluster_custom(dataMissLab, 0, classifiedThreshold, classifiedCertaintyThreshold, waitTime, vis); %(i)
        end
        %sum(dataCluster2.y ~= dataCluster2.lab(end))
        %iUnDetermined = classCertainty2(1,:) == classCertainty2(2,:);
        %pClassified = (~iUnDetermined * nSamples2')/n.tot;
        
        iClustered2 = dataCluster2.y ~= dataCluster2.lab(end);
        errorCluster2(i,ii) = sum(dataTrain.y(iClustered2) ~= dataCluster2.y(iClustered2))/length(dataTrain.y(iClustered2));
        
        %% LDA classify
        [wCluster1 , x0Cluster1, meanCluster1, varCluster1] = lda_train(dataCluster1);
        [wCluster2 , x0Cluster2, meanCluster2, varCluster2] = lda_train(dataCluster2);
        [wMissLab, x0MissLab, meanMissLab, varMissLab] = lda_train(dataMissLab);

        errorAppCluster1(i,ii) = lda_test(wCluster1, x0Cluster1, dataCluster1);
        errorAppCluster2(i,ii) = lda_test(wCluster2, x0Cluster2, dataCluster2);
        errorAppMissLab(i,ii) = lda_test(wMissLab, x0MissLab, dataMissLab);

        errorTrueCluster1(i,ii) = lda_test(wCluster1, x0Cluster1, dataTest);
        errorTrueCluster2(i,ii) = lda_test(wCluster1, x0Cluster2, dataTest);
        errorTrueMissLab(i,ii) = lda_test(wMissLab, x0MissLab, dataTest);

        %% likelihood
        llCluster1(i,ii) = log_likelihood(dataCluster1, meanCluster1, varCluster1);
        llCluster2(i,ii) = log_likelihood(dataCluster2, meanCluster2, varCluster2);
        llMissLab(i,ii) = log_likelihood(dataMissLab, meanMissLab, varMissLab);
        
        if vis
            disp(['Labels added: ', num2str(sum(iClustered1) - n.lab)]);
            disp(['Percentage labled: ', num2str(sum(iClustered1)/length(dataCluster1.y))]);

            disp(['Clustering error: ', num2str(errorCluster1(i))]);

            disp(['Apparent error original data: ', num2str(errorAppMissLab(i))]);
            disp(['Apparent error clustered data: ', num2str(errorAppCluster1(i))]);

            disp(['True error original data: ', num2str(errorTrueMissLab(i))]);
            disp(['Ture error clustered data: ', num2str(errorTrueCluster1(i))]);
        end
    end
end

%% clustering error
figure('Position', [200,200,1000,350])
semilogx(n.unlab, mean(errorCluster1,2), 'LineWidth', lineWidth)
hold on
semilogx(n.unlab, mean(errorCluster2,2), 'LineWidth', lineWidth)
xlabel('Unlabled data [-]')
ylabel('Error [-]')
axis([min(n.unlab), max(n.unlab), 0, 0.35])
grid minor
title('Clustering Error')
legend('Complete Linkage','Single Linkage','Location','southeast')

set(gca,'Fontsize',fontSize)
saveas(gca, ['fig/clusterError', saveLabel], 'jpg')
saveas(gca, ['fig/clusterError', saveLabel, '.eps'], 'epsc')

%% Likelihood
figure('Position', [200,200,1000,350])
plot(n.unlab, mean(llMissLab,2), 'LineWidth', lineWidth)
hold on
plot(n.unlab, mean(llCluster1,2), 'LineWidth', lineWidth)
hold on
plot(n.unlab, mean(llCluster2,2), 'LineWidth', lineWidth)
legend('Missing Label','Complete Linkage','Single Linkage','Location','southwest')
axis([min(n.unlab), max(n.unlab), min([ mean(llCluster1,2);  mean(llCluster2,2)]), 0])
grid minor
xlabel('Unlabled data [-]')
ylabel('Log Likelihood [-]')
set(gca,'Fontsize',fontSize)
title('Log Likelihood');

saveas(gca, ['fig/LL', saveLabel], 'jpg')
saveas(gca, ['fig/LL', saveLabel, '.eps'], 'epsc')

%% Apparent error
figure('Position', [200,200,1000,350])
subplot(1,2,1)
[fill1, fill2] = plot_error(errorAppMissLab,errorAppCluster1, n.unlab);

legend('Non-clustered data', 'Clustered data','Location','northeast')
uistack(fill1,'bottom')
uistack(fill2,'bottom')
title('Apperent Error Complete Linkage');
set(gca,'Fontsize',fontSize)
axis([min(n.unlab), max(n.unlab), 0, 0.3])

subplot(1,2,2)
[fill1, fill2] = plot_error(errorAppMissLab,errorAppCluster2, n.unlab);
legend('Non-clustered data', 'Clustered data','Location','northeast')
uistack(fill1,'bottom')
uistack(fill2,'bottom')
if lda
    title('Apperent Error (LDA)');
else
    title('Apperent Error Single Linkage');
end
set(gca,'Fontsize',fontSize)
axis([min(n.unlab), max(n.unlab), 0, 0.3])

saveas(gca, ['fig/appError', saveLabel], 'jpg')
saveas(gca, ['fig/appError', saveLabel, '.eps'], 'epsc')

%% True error
figure('Position', [200,200,1000,350])
subplot(1,2,1)
[fill1, fill2] = plot_error(errorTrueMissLab,errorTrueCluster1, n.unlab);

legend('Non-clustered data', 'Clustered data','Location','southeast')
uistack(fill1,'bottom')
uistack(fill2,'bottom')
title('True Error Complete Linkage')
set(gca,'Fontsize',fontSize)
axis([min(n.unlab), max(n.unlab), 0.1, 0.5])

subplot(1,2,2)
[fill1, fill2] = plot_error(errorTrueMissLab,errorTrueCluster2, n.unlab);
legend('Non-clustered data', 'Clustered data','Location','southeast')
uistack(fill1,'bottom')
uistack(fill2,'bottom')
if lda
    title('True Error (LDA)');
else
    title('True Error Single Linkage');
end
set(gca,'Fontsize',fontSize)
axis([min(n.unlab), max(n.unlab), 0.1, 0.5])

saveas(gca, ['fig/trueError', saveLabel], 'jpg')
saveas(gca, ['fig/trueError', saveLabel, '.eps'], 'epsc')

save(['res', filesep, saveLabel])