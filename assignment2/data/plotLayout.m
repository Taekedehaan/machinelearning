lineWidth = 3; %line width, used for graphs
fontSize = 14; %font size, used for graphs
colorBlue = [0, 0.4470, 0.7410]; % blue, used for plotting
colorGrey = 0.7*[1, 1, 1]; % grey, used for plotting
dim = [200, -200, 600, 600];
dim1 = [200, 200, 600, 600];
dimWide = [200, -200, 1200, 600];