function l = mixLoss(t, z, p)
    l = p(:,t) .* exp(-z(:,t));
    l = -log(sum(l));
end