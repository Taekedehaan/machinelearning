close all
clear all
clc

d = 3;
tEnd = 4;

p_A = zeros(d,tEnd);
p_B = zeros(d,tEnd);
L_B = nan(1,tEnd);
L_A = nan(1,tEnd);

% z_t^i is the loss of expert i at time t
z = [0.0, 0.0, 1.0, 0.0;
        0.1, 0.0, 0.0, 0.9;
        0.2, 0.1, 0.0, 0.0];

% strategy A
for t = 1:tEnd
    if t == 1
        p_A(:,1) = [1/3, 1/3, 1/3]';
    else
        [minVal, argmin] = min(sum(z(:,1:t-1),2));
        p_A(argmin, t) = 1;
    end
    L_A(1,t) = mixLoss(t, z, p_A);
end
LTot_A = sum(L_A);
R_A = LTot_A - min(sum(z(:,1:tEnd),2));

disp('=====A=====');
disp('p: ');
disp(p_A);
disp('mix loss/round: ')
disp(L_A);
disp(['total mix loss: ', num2str(LTot_A)] )
disp(['total regret: ', num2str(R_A)])

% strategy B
for t = 1:tEnd
     if t == 1
        p_B(:,1) = [1/3, 1/3, 1/3]';
    else
        L = sum(z(:,1:t-1),2);
        C = sum(exp(-L));
        p_B(:,t) = exp(-L)/C;
     end
     L_B(1,t) = mixLoss(t, z, p_B);
end
LTot_B = sum(L_B);
R_B = LTot_B - min(sum(z(:,1:tEnd),2));
C = min(sum(z(:,1:tEnd),2)) + log(d);

disp('======B=====');
disp('p: ');
disp(p_B);
disp('mix loss/round: ')
disp(L_B);
disp(['total mix loss: ', num2str(LTot_B)] )
disp(['total regret: ', num2str(R_B)])
disp(['Uppder bound cum mix loss: ', num2str(C)]);