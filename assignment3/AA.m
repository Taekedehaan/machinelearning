% Exercise: Aggregating Algorithm (AA)
clear all
close all
clc

fontSize = 15;
lineWidth = 1.5;

load coin_data;

d = size(s,2);
n = size(s,1);
p0 = ones(1,5)/5;

% compute adversary movez z_t
z = -log(r);

% compute strategy p_t (see slides)
L = cumsum(z,1); %cumlative loss of expert i up to t
C = sum(exp(-L),2); % normlaization
p = [p0; exp(-L)./C]; % AA strategy
p(end, :)= []; %clear last row, we only go until n

% compute loss of strategy p_t
l_AA  = -log(sum(p .* exp(-z),2));

% compute losses of experts
l_exp = z;

% compute regret
regret = cumsum(l_AA) - min(cumsum(l_exp), [], 2);

% compute total gain of investing with strategy p_t
c = cumprod(sum(p .* r, 2));

disp(['Total loss l_AA: ', num2str(sum(l_AA,1))]);
disp(['Total loss of expert e_i: ', num2str(sum(l_exp,1))]);
disp(['Increse in wealth: ', num2str(c(end))]);
disp(['Regret: ', num2str(regret(end))]);
%% plot of the strategy p and the coin data

figure('Position', [200,200,1000,400])
subplot(1,2,1);
plot(p, 'Linewidth', lineWidth)
legend(symbols_str)
title('rebalancing strategy AA')
xlabel('date')
ylabel('confidence p_t in the experts')

grid minor
set(gca,'Fontsize',fontSize)

subplot(1,2,2);
semilogy(s, 'Linewidth', lineWidth)
legend(symbols_str)
title('worth of coins')
xlabel('date')
ylabel('USD')

grid minor
set(gca,'Fontsize',fontSize)

saveas(gca, 'fig/AA_p', 'jpg')
saveas(gca, 'fig/AA_p.eps', 'epsc')
%% Plot regret
figure('Position', [200,200,1000,400])
subplot(1,2,1);
plot(1:n, regret, 'Linewidth', lineWidth)
hold on
plot(1:n, repmat(log(d), 1, n), 'Linewidth', lineWidth)
title('Regret')
xlabel('date')
ylabel('regret')
legend('Regret AA','Theoretical maximum', 'Location', 'northeast')
axis([1, n, 0, 1.7])
grid minor
set(gca,'Fontsize',fontSize)

subplot(1,2,2);
plot(c, 'Linewidth', lineWidth)
title('Increse in Wealth')
xlabel('date')
ylabel('Increse in Wealth')
grid minor
set(gca,'Fontsize',fontSize)

saveas(gca, 'fig/AA_regret', 'jpg')
saveas(gca, 'fig/AA_regret.eps', 'epsc')