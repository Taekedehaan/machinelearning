% Exercise: Online Gradient Descent (OGD)
clear all;
close all
clc

fontSize = 15;
lineWidth = 1.5;
error = 0.00001;
load coin_data;

a_init = [0.2, 0.2, 0.2, 0.2, 0.2]'; % initial action

n = 213; % is the number of days
d = 5; % number of coins

% we provide you with values R and G.
alpha = sqrt(max(sum(r.^2,2))); 
epsilon = min(min(r)); 
G = alpha/epsilon; 
R = 1; 

% set eta:
eta = R/(G * sqrt(n));

a = a_init; % initialize action. a is always a column vector

L_OGD = nan(n,1); % keep track of all incurred losses
A = nan(d,n); % keep track of all our previous actions

for t = 1:n
    
    % we play action a
    [l,g] = mix_loss(a,r(t,:)'); % incur loss l, compute gradient g
    
    A(:,t) = a; % store played action
    L_OGD(t) = l; % store incurred loss
    
    % update our action, make sure it is a column vector
    a = a - eta * g;
    
    % after the update, the action might not be anymore in the action
    % set A (for example, we may not have sum(a) = 1 anymore). 
    % therefore we should always project action back to the action set:
    a = project_to_simplex(a')'; % project back (a = \Pi_A(w) from lecture)

end

% compute total loss
disp(['Total loss l_OGD: ', num2str(sum(L_OGD))]);

% compute best fixed strategy (you may make use of loss_fixed_action.m and optimization toolbox if needed)
z = -log(r);
L_a = z;

a_fix = a_init;
while true
    [L_fix_old, gradOld] = loss_fixed_action(a_fix);
    a_fix = a_fix - eta*gradOld;
    a_fix = project_to_simplex(a_fix')';
    
    [L_fix_new, gradNew] = loss_fixed_action(a_fix);
    if abs(L_fix_new-L_fix_old) < error
        L_fix = L_fix_new;
        break
    end
end

% compute regret 
regret = sum(L_OGD) - L_fix;
regret_lim = R * G * sqrt(n);

% compute total gain in wealth
c = cumprod(sum(A' .* r, 2));

%% disp
disp(['Total loss 1 fixed action: ', num2str(sum(L_a,1))]);
disp(['Best fixt action: ', num2str(a_fix')]);
disp(['Best fixt action loss: ', num2str(L_fix)]);
disp(['Increse in wealth: ', num2str(c(end))]);
disp(['Regret at n: ', num2str(regret(end))]);
disp(['Regret limit: ', num2str(regret_lim)]);

%% plot of the strategy A and the coin data
figure('Position', [200,200,1000,400])
subplot(1,2,1);
plot(A', 'Linewidth', lineWidth)
legend(symbols_str)
title('rebalancing strategy AA')
xlabel('date')
ylabel('confidence p_t in the experts')
grid minor
set(gca,'Fontsize',fontSize)

subplot(1,2,2);
semilogy(s, 'Linewidth', lineWidth)
legend(symbols_str)
title('worth of coins')
xlabel('date')
ylabel('USD')
grid minor
set(gca,'Fontsize',fontSize)

saveas(gca, 'fig/OGD_p', 'jpg')
saveas(gca, 'fig/OGD_p.eps', 'epsc')

%% Plot regret
figure('Position', [200,200,500,400])
plot(c, 'Linewidth', lineWidth)
title('Increse in Wealth')
xlabel('date')
ylabel('Increse in Wealth')
grid minor
set(gca,'Fontsize',fontSize)

saveas(gca, 'fig/OGD_regret', 'jpg')
saveas(gca, 'fig/OGD_regret.eps', 'epsc')