function [weightedError, label] = classify_decision_stump(data, decisionStump, varargin)
           
% check if input is okay
if length(varargin) > 1
    error('Too many input arguments');
end

[f, y, t] = unpack_decision_stump(decisionStump);

% classify
if y == 1 % > larger than threshold get label 1
    % > larger than threshold get label 1
    i1 = data.x(:,f) > t;
    label = data.lab(1) * i1 + data.lab(2) * (~i1);
elseif y == 2 % <
    % < smaller than threshold get label 1
    i1 = data.x(:,f) < t;
    label = data.lab(1) * i1 + data.lab(2) * (~i1);
else
    error('Invalid decision stump input');
end

if ~isempty(varargin)
    % if inputs weights are given compute weighted error
    p = varargin{1};
    weightedError = sum(p .*(label ~= data.y));
else
    % if no input weights given return indexes INCORRECTLY classified obj.
    weightedError = label ~= data.y;
end


end