function [l1, l2] = plot_data(data, w) 
% data struct containing:
% - data.x: data
% - data.y: data classes
% - data.lab: possible lables

    if length(w) < data.n
        w = zeros(data.n,1);
    else
        w = w/max(w);
    end
    P = 4; % constant used for color mapping

    % split in both classes
    yC1 = data.y == data.lab(1);
    yC2 = data.y == data.lab(2);

    for i = 1:data.n

        if any(i == find(yC1))
            color = map2color(w(i), 'b');
            l1 = plot(data.x(i,1), data.x(i,2), 'o', 'Color', color);
        else
            color = map2color(w(i), 'r');
            l2 = plot(data.x(i,1), data.x(i,2), 'o', 'Color', color);
        end
        
    end

    xlabel('feature 1')
    ylabel('feature 2')

   
end