function [decisionStumpUnique, weightUnique] = get_unique_decision_stump(decisionStump, beta)

for i = 1:length(decisionStump)
    [f(i), y(i), t(i)] = unpack_decision_stump(decisionStump(i));
end

weight = log(1./beta);
mat = [f', y', t'];
[uniqueMat, IA, IC] = unique(mat, 'rows');
weightUnique = zeros(size(uniqueMat,1), 1);

for i = 1:length(decisionStump)
    weightUnique(IC(i)) = weightUnique(IC(i)) + weight(i);
end

for i = 1:size(weightUnique,1)
    decisionStumpUnique(i) = pack_decision_stump(uniqueMat(i,1), uniqueMat(i,2), uniqueMat(i,3));
end