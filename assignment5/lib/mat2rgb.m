function B = mat2rgb(A)

    %rgb = mat2img(dataPlot.x(i,:)), 1, 1, 3);

    % B = mat2gray(reshape(A,[8,8])',[0,255]);

    B = repmat(mat2gray(A,[0,255]), 1, 1, 3);
end