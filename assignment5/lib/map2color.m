function color = map2color(w, rgb)

P = 4 * max(1); % constant used for color mapping

if rgb == 'r'
    color = [1, 0.6, 0.6]; % green
    color = color - 0.6 * [0, sin(w * 2 * pi / P), sin(w * 2 * pi / P)];
elseif rgb == 'g'
    color = [0.6, 1, 0.6]; % green
    color = color - 0.6 * [sin(w * 2 * pi / P), 0, sin(w * 2 * pi / P)];
elseif rgb == 'b'
    color = [0.6, 0.6, 1]; % green
    color = color - 0.6 * [sin(w * 2 * pi / P), sin(w * 2 * pi / P), 0];
elseif rgb == 'l'
    color = [0.8, 0.8, 0.8]; % green
    color = color - 0.8 * [sin(w * 2 * pi / P), sin(w * 2 * pi / P), sin(w * 2 * pi / P)];
end