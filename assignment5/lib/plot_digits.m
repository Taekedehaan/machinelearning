function plot_digits(data, decisionStump, dim)

color1 = [0.5, 0.5, 1];
color2 = [1, 0.5, 0.5];

figure('Position', [100, 100, 1000, 600])
    for i = 1:data.n
        subplot(dim(1),dim(2),i)
        
        % conver mat to rgb
        rgb = mat2rgb(data.x(i,:));
        
        for ii = 1:length(decisionStump)
            [f, y, t] = unpack_decision_stump(decisionStump(ii));
            if y == 1
                % above in class 1
                if data.x(i,f) > t 
                    color = color1;
                else
                    color = color2;
                end
            else
                if data.x(i,f) < t 
                    color = color1;
                else
                    color = color2;
                end
            end
            rgb(1,f,:) = color;
        end
        rgb = permute(reshape(rgb,[8,8,3]),[2, 1, 3]);
        
        imshow(rgb,'InitialMagnification','fit');
    end