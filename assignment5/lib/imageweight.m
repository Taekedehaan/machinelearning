function imageweight()

%% show image weights
if ~plotData
    typical0 = zeros(1,64);
    
    % loop over all the decision stumps
    for i = 1:itterations
        weightCurent = log(1./beta(i));
        featCurrent = feat(i);
        if(y(i) == 1)
            typical0(featCurrent) = 1 + typical0(featCurrent);
        elseif (y(i) == 2)
            typical0(featCurrent) = 0 + typical0(featCurrent);
        end
    end

    rgb = mat2img(typical0/max(typical0));
    rgbPlot = permute(reshape(rgb,[8,8,3]),[2, 1, 3]);
    imshow(rgbPlot,'InitialMagnification','fit');

end