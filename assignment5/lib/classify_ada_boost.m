function [label, error] = classify_ada_boost(dataTest, beta, decisionStump)


% init
objectScore = 0;
itterations = length(decisionStump);

% compute threshold
threshold = 1/2 * sum(log(1./beta(1:itterations)));

% compute object score
for t = 1:itterations
    [~, h_t] = classify_decision_stump(dataTest, decisionStump(t), 0);
    objectScore = objectScore + log(1/beta(t)) * (h_t - 1);
end

% classify objects
i1 = objectScore > threshold;
label(i1,1) = dataTest.lab(2);
label(~i1,1) = dataTest.lab(1);

% compute error
error = sum(abs(label ~= dataTest.y))/dataTest.n;

end