function plot_decision_stump(decisionStump)
    % t = threshold
    for i = 1:length(decisionStump)
        [f(i), y(i), t(i)] = unpack_decision_stump(decisionStump(i));
    end
    
    % plot classifier
    color1 = [0.95, 0.95, 1];
    color2 = [1, 0.95, 0.95];
    
    if f == 1 % vertical
        y1 = [-5, 5, 5, -5];
        y2 = [-5, 5, 5, -5];
        % plot([t, t], [-5, 5], '--k', 'LineWidth', 1)
        if y == 1
            % right is class 1
            x1 = [t, t, 5, 5];
            
            % left is class 2
            x2 = [-5, -5, t, t] ;
        else
            % left is class 1
            x1 = [-50, -50, t, t];
            
            % right is class 2
            x2 = [t, t, 50, 50];
        end
    elseif f == 2
        x1 = [-5, 5, 5, -5];
        x2 = [-5, 5, 5, -5];
        if y == 1
            % above in class 1
            y1 = [t, t, 5, 5];
            
            % below is class 2
            y2 = [-5, -5, t, t];
        else
            % below is class 1
            y1 = [-5, -5, t, t];
            
            % above in class 2
            y2 = [t, t, 5, 5];
        end
        
        % plot([-5, 5], [t, t], '--k', 'LineWidth', 1)    
    end
    fill(x1,y1, color1, 'LineWidth', 1) 
    fill(x2,y2, color2, 'LineWidth', 1) 

end