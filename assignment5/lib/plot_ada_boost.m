function plot_ada_boost(decisionStump, w)

% w = log(1./beta);
w = w/max(w);

for i = 1:length(decisionStump)
    [f(i), y(i), t(i)] = unpack_decision_stump(decisionStump(i));
end

% plot classifier
    for i = 1:length(t)
        
        color = map2color(w(i), 'l');
        if f(i) == 1
            plot([t(i), t(i)], [-5, 5], 'Color', color, 'Linewidth', 2)
        elseif f(i) == 2
            plot([-5, 5], [t(i), t(i)], 'Color', color, 'Linewidth', 2) % '--',
        end
    end
end