function B = mat2img(A)
    B = mat2gray(reshape(A,[8,8])',[0,255]);
end