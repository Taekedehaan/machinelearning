function [decisionStumpStar, errorStore, T] = train_decision_stump(data, varargin)

if length(varargin) > 1
    error('Too many input arguments');
end

% init
Y = [1, 2]; 
F = 1:size(data.x,2); 
errorMin = realmax('double');

% loop over different features
for f = F
    % loop over different signs
    for y = Y     
        
        count = 0;
        T =  unique(data.x(:,f))';
        % for the different thresholds, can be vectorized!
        for t = T
            count = count + 1;
            
            % compute error
            decisionStump = pack_decision_stump(f, y, t);
            
            % if inputs weights are given compute weighted error
            [errorTrain, ~] = classify_decision_stump(data, decisionStump, varargin{1});

            % if new optimum reached
            if errorTrain < errorMin
                errorMin = errorTrain;
                decisionStumpStar = pack_decision_stump(f, y, t);
            end
            
            errorStore(f, count) = errorTrain;
            Tstore(f,count) = t;
        end
    end
    
    
end
