clear all
close all
clc

%% init
nTrain = 100; % number of training samples
dataType = 'digit';
itterations = 50; % [1, 10, 20, 50, 100, 200];
nRepetitions = 2;
debug = false;

%% data
switch dataType
    case 'simple'
        plotData = true;
        distance = 2;
        n = 1125;
        data = load_simple_data(n, distance);
        data = update_struct(data);
        axisLim = [-2, 4, -5, 3];
    case 'hard'
        plotData = true;
        n = 1125;
        data = load_hard_data(n);
        data = update_struct(data);
        axisLim = [-5, 4, -5, 3];
    case 'digit'
        data = load_opt_digit();
        data = update_struct(data);
        plotData = false;
end


%% Train adaboost
for i = 1:length(itterations)
    
    for ii = 1:nRepetitions
        % split data
        [dataTest, dataTrain] = split_data(data, nTrain/2);

        % train ada boost
        [decisionStump, beta, w] = train_adabootst(dataTrain, itterations(i));

        % get result ADAboost
        [~, errorADAboost(i,ii)] = classify_ada_boost(dataTest, beta, decisionStump);
        
        % get result decision stump
        [temp, ~] = classify_decision_stump(dataTest, decisionStump(1));
        errorDecisionStump(i,ii) = sum(temp)/length(temp);
        
        
        if true
            disp(['Decision stump Test error: ', num2str(errorDecisionStump(i,ii))]);
            disp(['Adaboost Test error: ', num2str(errorADAboost(i,ii))]);
        end
    end
end

[decisionStumpUnique, weightUnique] = get_unique_decision_stump(decisionStump, beta);

%% show error
meanErrorAda = mean(errorADAboost,2);
stdErrorAda = std(errorADAboost, [],2);

meanErrorDecisionStump = mean(errorDecisionStump,2);
stdErrorDecisionStump = std(errorDecisionStump, [],2);

figure('Position', [200, 200, 1000 400])
hold on

errorbar(itterations, meanErrorAda, stdErrorAda, 'Linewidth', 2)
errorbar(itterations, meanErrorDecisionStump, stdErrorDecisionStump, 'Linewidth', 2)

legend('AdaBoost','Decision stump')

title('Error Rate')
xlabel('itterations [-]')
ylabel('error rate [-]')
set(gca,'XScale','log')
if ~(length(itterations) == 1), xlim([min(itterations), max(itterations)]); end

saveas(gca,'fig/error', 'jpg')
saveas(gca,'fig/error', 'epsc')


%% show difficult images
[~, iMax] = maxk(w,4);
dataPlot.x = dataTrain.x(iMax,:);
dataPlot.y = dataTrain.y(iMax,:);
dataPlot = update_struct(dataPlot);

figure
dim = [2,2];

if ~plotData
    plot_digits(dataPlot, decisionStumpUnique, dim)
    
    
    figure('Position', [100, 100, 1000, 600])
    for i = 1:dataPlot.n
        subplot(dim(1),dim(2),i)

        % conver mat to rgb
        rgb = mat2rgb(dataPlot.x(i,:));
        rgb = permute(reshape(rgb,[8,8,3]),[2, 1, 3]);

        imshow(rgb,'InitialMagnification','fit');
        saveas(gca,'fig/hard', 'epsc')
    end
end

%% plot colored digits
[~, dataPlot] = split_data(dataTest, 16);
dim = [4,8];

if ~plotData
    plot_digits(dataPlot, decisionStumpUnique, dim)
end
saveas(gca,'fig/digits2', 'jpg')
saveas(gca,'fig/digits2', 'epsc')


%% plot ada boost with data
if plotData
    figure
    hold on
    plot_ada_boost(decisionStumpUnique, weightUnique);
    [l1, l2] = plot_data(dataTrain, w);
    axis(axisLim)
    legend([l1, l2], 'class A', 'class B')
    
    saveas(gca,'fig/ada', 'jpg')
    saveas(gca,'fig/ada', 'epsc')
end


%% function declarations
function data = load_opt_digit()
    load optdigitsubset.txt
    data.x = optdigitsubset;
    data.lab = [1, 2];
    data.y(1:554,1) = data.lab(1);
    data.y(555:1125,1) = data.lab(2);
end

function data = load_simple_data(n, distance)
    N = [round(n/2), round(n/2)];
    K = 2; % features
    LABTYPE = 'crisp';
    prData = gendats(N,K ,distance,LABTYPE);

    data.x = +prData;
    data.y = struct(prData).nlab;
    data.lab(1) = 1;
    data.lab(2) = 2;
end

function data = load_hard_data(n)

N = [round(n/2), round(n/2)];
prData = gendatb(N);

data.x = +prData/2.5;
data.y = struct(prData).nlab;
data.lab(1) = 1;
data.lab(2) = 2;
end

% Goal to have the same implemntation in the paper
% Optimizations are not very important, if you find something cool that
% would be nice.

% Give the threshold direction and feaure (be complete).

% The exercises are made in a way so he can chack the answers.

% for exercise c repeat it multiple times for more accurate results.

% class priors may be asumed 50/50, but what would happen if they
% completely change.

% Th peformence will change a little bit when using>, >=.

% How to draw a desion doundary for adaboost
%   - prtools prmapping
%   3 things:
%         empymapping (empty shell)
%         how do you train the mapping
%         how do you evaluate the newly incomming object
% if you dont want to do that: make a scatter plot of the dataset
% take a large number of points in this feauture spce and plot these
% can be very slow

% it is a two class classifier