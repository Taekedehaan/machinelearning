clear all
close all
clc

% %% init
nTrain = 50; % per class
simple = true;
nTests = 1;
color1 = [0.5, 0.5, 1];
color2 = [1, 0.5, 0.5];
% % init weight
w = ones(2 * nTrain,1);
% 
% %% data
if simple
    distance = 2;
    n = 1000;
    data = load_simple_data(n, distance);
    data = update_struct(data);
else
    data = load_opt_digit();
    dataTest = update_struct(data);
end

for i = 1:nTests
    % split data
    [dataTest, dataTrain] = split_data(data, nTrain);

    % update weight
    % index = dataTrain.x(:,1) < 0 & dataTrain.y == dataTrain.lab(2);
    % w(index) = 100;
    
    %% train classifier
    [f(i), y(i), t(i), errorStore, T] = train_decision_stump(dataTrain, w);
    
    % test error
    label = classify_decision_stump(dataTest, f(i), t(i), y(i));
    errorTest(i) = sum(label ~= dataTest.y)/length(dataTest.y);
    
end
disp(['mean f: ', num2str(mean(f)), ' with std: ', num2str(std(f))]);
disp(['mean y: ', num2str(mean(y)), ' with std: ', num2str(std(y))]);
disp(['mean t: ', num2str(mean(t)), ' with std: ', num2str(std(t))]);
disp(['error: ', num2str(mean(errorTest)), ' with std: ', num2str(std(errorTest))]);

%% plot chosen f
fUnique = unique(f);
fCount = zeros(size(fUnique));

for i = 1:length(fUnique)
    fCount(i) = sum(f==fUnique(i));
end
figure('Position', [100, 100, 1000, 400])
bar(fUnique, fCount/nTests)
xlabel('feature number')
ylabel('frequency')
title('Distibution of chosen features ')

% save
saveas(gca, 'fig/feature.jpg')
saveas(gca, 'fig/feature.eps', 'epsc')

% get final values
f = f(end);
y = y(end);
t = t(end);

%% plot classifier
[~, dataPlot] = split_data(dataTest, 16);
if ~simple
    figure('Position', [100, 100, 1000, 600])
    for i = 1:dataPlot.n
        subplot(4,8,i)
        
        % conver mat to rgb
        rgb = mat2img(dataPlot.x(i,:));
        
        if y == 1
            % above in class 1
            if dataPlot.x(i,f) > t 
                color = color1;
            else
                color = color2;
            end
        else
            if dataPlot.x(i,f) < t 
                color = color1;
            else
                color = color2;
            end
        end
        rgb(1,f,:) = color;
        rgb = permute(reshape(rgb,[8,8,3]),[2, 1, 3]);
        
        imshow(rgb,'InitialMagnification','fit');
    end
end

% save
saveas(gca, 'fig/digit.jpg')
saveas(gca, 'fig/digit.eps', 'epsc')

%% Errors
% train error
label = classify_decision_stump(dataTrain, f, t, y);
errorTrain = sum(label ~= dataTrain.y)/length(dataTrain.y);
disp(['train error: ', num2str(errorTrain)])

%% show results
if simple
    %% plot error
    figure
    hold on

    title('error for different thresholds')
    plot(T, errorStore)
    legend('> for class A', '< for class A')
    xlabel('threshold');
    ylabel('error');
end

if simple
    %% train
    figure
    hold on
    
    plot_classifier(f, y, t)
    plot_data(dataTrain, w)
    
    grid minor
    set(gca,'layer','top');
    
    title('Clasifier with training data')
    saveas(gca, 'fig/weak_learner_train.jpg')
    saveas(gca, 'fig/weak_learner_train.eps', 'epsc')
    
    %% test
    figure
    hold on
    
    plot_classifier(f, y, t)
    plot_data(dataTest, w)
    
    grid minor
    set(gca,'layer','top');
    
    title('Clasifier with test data')
    saveas(gca, 'fig/weak_learner_test.jpg')
    saveas(gca, 'fig/weak_learner_test.eps', 'epsc')
end

function data = load_opt_digit()
    load optdigitsubset.txt
    data.x = optdigitsubset;
    data.lab = [1, 2];
    data.y(1:554,1) = data.lab(1);
    data.y(555:1125,1) = data.lab(2);
end

function data = load_simple_data(n, distance)
   
    N = [round(n/2), round(n/2)];   % samples per class
    K = 2;                          % features
    LABTYPE = 'crisp';
    prData = gendats(N,K ,distance,LABTYPE);

    data.x = +prData;
    % data.x(:,1) = 10 * data.x(:,1);
    data.y = struct(prData).nlab;
    data.lab(1) = 1;
    data.lab(2) = 2;
end