function [decisionStump, beta, w] = train_adabootst(data, itterations)

% init weight
w = ones(data.n,1);
beta = nan(1,itterations);

for t = 1:itterations

    % normalize
    p = w/(sum(w));    
    
    %% train classifier
    [decisionStump(t), ~, ~] = train_decision_stump(data, p);

    % weigthed error
    [weightedError, ~] = classify_decision_stump(data, decisionStump(t), p);
    
    % non weighted error
    errorTrain = classify_decision_stump(data, decisionStump(t));
    
    beta(t) = weightedError / (1 - weightedError);

    if weightedError == 0
        break;
    end
    
    if false
        disp(['weighted error: ', num2str(weightedError)]);
        disp(['unweighted error: ' num2str(sum(errorTrain)/length(errorTrain))]);
    end
    
    % update weights
    w = w .* beta(t).^(1 - errorTrain);
    
end    

end