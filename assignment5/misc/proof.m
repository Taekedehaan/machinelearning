clear all
close all
clc

x = -2:0.001:2;
y1 = exp(-x);
y2 = (1 - x);

figure
semilogy(x, y1)
hold on
semilogy(x, y2)
legend('e^{-x}', '1-x')

figure
plot(x, y1)
hold on
plot(x, y2)
legend('e^{-x}', '1-x')