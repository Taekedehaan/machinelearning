function [decisionStumpStar] = train_decision_stump(data)

% init
Y = [1, 2]; 
F = 1:size(data.x,2); 
errorMin = realmax('double');

% loop over different features
for f = F
    % loop over different signs
    for y = Y     
        
        T =  unique(data.x(:,f))';
        % for the different thresholds, can be vectorized!
        for t = T
            count = count + 1;
            
            % classify
            if y == 1 
                % > larger than threshold get label 1
                i1 = data.x(:,f) > t;
                label = data.lab(1) * i1 + data.lab(2) * (~i1);
            elseif y == 2 
                % < smaller than threshold get label 1
                i1 = data.x(:,f) < t;
                label = data.lab(1) * i1 + data.lab(2) * (~i1);
            end

            error = (label ~= data.y)/data.n;
            
            % if new optimum reached
            if error < errorMin
                % store new minimum error
                errorMin = error;
                
                % store classifier in struct
                decisionStumpStar = pack_decision_stump(f, y, t);
            end
        end
    end
end
