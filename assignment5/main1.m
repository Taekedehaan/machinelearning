clear all
close all
clc

%% init
N = [50,50];
K = 2;
D = 2; % distance
LABTYPE = 'crisp';
lab1 = 1;
lab2 = 2;
%% gen data
prData = gendats(N,K ,D,LABTYPE);
data = +prData;
lab = struct(prData).nlab;

%% train classifier
% choose feature
% choose threshold

% init
Y = [1, 2]; 
F = 1; %[1, 2]; 
errorMin = realmax('double');

count = 0;

% for the different features
for feature = F
    T =  unique(data(:,feature))';
    % for the different signs
    for y = Y     
        % for the different thresholds, can be vectorized!
        for t = T
            count = count + 1;
            
            % classify
            if y == 1 % >
                classify = lab1 * (data(:,feature) > t) + lab2 * (data(:,feature) <= t);
            elseif y == 2 % <
                classify = lab1 * (data(:,feature) < t) + lab2 * (data(:,feature) >= t);
            end
            
            % compute error
            error = sum(abs(classify - lab));
            
            % if new optimum reached
            if error < errorMin
                errorMin = error;
                fStar = feature;
                yStar = y;
                tStar = t;
            end
            
            errorStore(count) = error;
        end
    end
end

%% plot error
figure
hold on

title('error for different thresholds')
plot(T, errorStore(1:100))
plot(T, errorStore(101:200))
legend('> for class A', '< for class A')
xlabel('threshold');
ylabel('error');

%% show results
figure
hold on
plot(data(lab == lab1,1), data(lab == lab1,2), 'o');
plot(data(lab == lab2,1), data(lab == lab2,2), 'o');

xlabel('feature 1')
ylabel('feature 2')

if fStar == 1
    plot([tStar, tStar], [-5, 5])
elseif fStar == 2
    plot([-5, 5], [tStar, tStar])    
end
    
legend('class A', 'class B')