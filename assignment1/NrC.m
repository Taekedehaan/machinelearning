clear all
close all
clc

%% Layout
lineWidth = 2; %line width, used for graphs
fontSize = 14; %font size, used for graphs
colorBlue = [0, 0.4470, 0.7410]; % blue, used for plotting
colorGrey = 0.9*[1, 1, 1]; % grew, used for plotting
dim = [200,200,800,300];

lambda = (0:1:3)';
r = -1:0.001:2;
%L = 1/2 * (1 + r).^2 + 1/2 *(1 - r).^2 + lambda .* abs(1 - r);
L = r.^2 + 1 + lambda .* abs(1 - r);
diffL = 2 * r + lambda .* (r-1)./(abs(1-r));

LdiffNum = diff(L,1,2)./diff(r);

lambdaCom = (-2 * r .* abs(1 - r))./(r - 1);

figure('Position',dim);
plot(r,L,'Linewidth',lineWidth)
xlabel('r_+')
ylabel('L')
legend(repmat("\lambda = ",4,1) + num2str(lambda))
grid minor;
box off;
title('Loss function')
set(gca,'fontsize', fontSize + 3);
saveas(gcf,'fig/loss1.eps', 'epsc')

%% derivaitve plot
figure('Position',dim);
plot(r,diffL,'Linewidth',lineWidth)
hold on
plot([min(r), max(r)], [0,0],'--','Color','r','Linewidth',lineWidth)
xlabel('r_+')
ylabel('dL/dr_+')
legend(repmat("\lambda = ",4,1) + num2str(lambda))
grid minor;
box off;
title('Derivative of loss function')
set(gca,'fontsize', fontSize + 3);
saveas(gcf,'fig/diffLoss.eps', 'epsc')

figure('Position',dim)
plot(lambdaCom, r,'Linewidth',lineWidth)
xlabel('lambda')
ylabel('r_+')
grid minor;
box off;
title('OPtimal r for selected lambda')
set(gca,'fontsize', fontSize + 3);
saveas(gcf,'fig/r.eps', 'epsc')

%% minimizer
lambda = 0:3;
r = [0, 0.5, 1, 1];
L = r.^2 + 1 + lambda .* abs(1 - r);