close all
clear all
clc

%% init
% parmeters
d = 64;
lambdas = [0, 0.01:0.01:0.09, 0.1:0.1:0.9, 1:1:9, 10:10:90, 100:100:1000]; %10000;%
rep = 10000; %10000; %

pTrain = 1;

% layout
lineWidth = 3;
fontSize = 20;
colorBlue = [0, 0.4470, 0.7410]; % blue, used for plotting
colorGrey = 0.9*[1, 1, 1]; % grew, used for plotting
dim = [200,200,800,300];

% load data 
fileName = 'optdigitsubset.txt';
fileID = fopen(fileName);

% read trext file and convert to double
A = textscan(fileID,repmat('%d' ,1,d)); 
x = double(cell2mat(A));

% define indexes of zero's and once's
nTot = length(x);
n = [554, 571];

% split data in two sets
xZero = x(1:n(1),:);
xOne = x((n(1)  + 1):nTot,:);

% label data
y = [ones(n(1), 1);2*ones(nTot - n(1), 1)];

count = 0;

pCorrectTrue = nan(rep, length(lambdas));
pErrorTrue = nan(rep, length(lambdas));

pCorrectApp = nan(rep, length(lambdas));
pErrorApp = nan(rep, length(lambdas));

data = [x, y];

for lambda = lambdas
    count = count + 1;
    disp(['progress: ', num2str(count/length(lambdas))]); 
    for i = 1:rep
        [xTest,yTest, xTrain, yTrain] = split_data(data, pTrain); 
        [r,L, rLength] = NrC_train(xTrain, yTrain, lambda);
        
        % true error
        [pCorrectTrue(i, count), pErrorTrue(i, count)] = NrC_test(xTest, yTest, r);
        
        % Apperent error
        [pCorrectApp(i, count), pErrorApp(i, count)] = NrC_test(xTrain, yTrain, r);
    end
end

%% Figures
% Cost
figure('Position',dim);
semilogy(L,'Linewidth',lineWidth)
grid minor
xlabel('Itteration [-]')
ylabel('Cost [-]')
set(gca,'fontsize', fontSize);
saveas(gcf,'fig/itteration', 'jpg')

errorTrueMean = mean(pErrorTrue,1);
errorTrueVar = var(pErrorTrue,1);
errorAppMean = mean(pErrorApp,1);

if length(lambdas) > 1
    figure('Position',dim);
    semilogx(lambdas, errorTrueMean, 'Linewidth',lineWidth)
    hold on
    semilogx(lambdas, errorAppMean, 'Linewidth',lineWidth)
    grid minor
    legend('True error', 'Apparent error')
    axis([min(lambdas), max(lambdas), 0,0.2])
    xlabel('\lambda [-]')
    ylabel('Error [-]')
    title('Regularization curve')
    set(gca,'fontsize', fontSize);
    saveas(gcf,'fig/errorLog.eps', 'epsc')
    saveas(gcf,'fig/errorLog', 'jpg')
end

if length(lambdas) > 1
    figure('Position',dim);
    loglog(lambdas, errorTrueMean, 'Linewidth',lineWidth)
    hold on
    plot(lambdas, errorAppMean, 'Linewidth',lineWidth)
    grid minor
    legend('True error', 'Apparent error')
    axis([min(lambdas), max(lambdas), 0,0.2])
    xlabel('\lambda [-]')
    ylabel('Error [-]')
    title('Regularization curve')
    set(gca,'fontsize', fontSize);
    saveas(gcf,'fig/error.eps', 'epsc')
    saveas(gcf,'fig/error', 'jpg')
end

%% plot some dimensions
nStart = 5;
nEnd = nStart + 1;

% plot
figure
plot(x(1:n(1),nStart),x(1:n(1),nEnd), 'o');
hold on
plot(r(1,nStart),r(1,nEnd),'o')
hold on
plot(x((n(1)+1):sum(n),nStart),x((n(1)+1):sum(n),nEnd), '*')
hold on
plot(r(2,nStart),r(2,nEnd),'*')
legend('0','r0','1','r1');

%% weigthts
figure
subplot(1,2,1)
title('Zero vector')
imshow(mat2img(r(1,:)),'InitialMagnification','fit');
subplot(1,2,2)
title('One vector')
imshow(mat2img(r(2,:)),'InitialMagnification','fit');
saveas(gcf,"fig/rVis" + num2str(lambda), 'jpg')
saveas(gcf,"fig/rVis" + num2str(lambda), 'eps')

% figure
% for i = 1:100
%     subplot(10,10,i)
%     imshow(mat2img(x(i+500,:)),'InitialMagnification','fit');
% end
