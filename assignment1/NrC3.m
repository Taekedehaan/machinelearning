clear all
close all
clc

%% Layout
lineWidth = 2; %line width, used for graphs
fontSize = 14; %font size, used for graphs
colorBlue = [0, 0.4470, 0.7410]; % blue, used for plotting
colorGrey = 0.9*[1, 1, 1]; % grew, used for plotting

%lambda = 1;
lambda = 0:1:3;
lambda = reshape(lambda,[1,1,length(lambda)]);

dr = 0.01;
r1 = -1:(dr):2;
r2 = (-1:dr:2)';
L = (r1.^2 + 1) + (r2.^2 - 2 * r2 + 5) + lambda .* abs(r2 - r1);

LdiffNum = diff(L,1,2)./diff(r1);

%lambdaCom = (-2 * r1 .* abs(1 - r1))./(r1 - 1);

for i = 1:length(lambda)
    Lcurrent = L(:,:,i);
    [LMin, locMin] = min(Lcurrent(:));
    [r2Min(i), r1Min(i)] = ind2sub(size(Lcurrent),locMin);
end


figure('Position',[200,-200,1600,400]);
for i = 1:length(lambda)
    nPlots = length(lambda);
    subplot(1,nPlots,i)
    contourf(r1, r2',L(:,:,i)) %,'Linewidth',lineWidth)
    hold on
    %plot(r1(r1Min(i)), r2(r2Min(i)),'o','Linewidth',lineWidth)
    xlabel('r_+');
    ylabel('r_-');
    grid minor;
    box off;
    title("Loss function (\lambda = " + num2str(lambda(i)) + ")"); 
    set(gca,'fontsize', fontSize + 3);
    
    ax = gca;
    outerpos = ax.OuterPosition;
    ti = ax.TightInset; 
    left = outerpos(1) + ti(1);
    bottom = outerpos(2) + ti(2) + .1;
    ax_width = outerpos(3) - ti(1) - ti(3);
    ax_height = outerpos(4) - ti(2) - ti(4) - .13;
    ax.Position = [left bottom ax_width ax_height];
    
    
end
saveas(gcf,'fig/NrC3loss.eps', 'epsc');

figure
plot(r1(r1Min), r2(r2Min),'Linewidth',lineWidth)
axis([min(r1), max(r1), min(r2), max(r2)]);

% figure('Position',[200,200,1000,500]);
% plot(r1(2:end),LdiffNum,'Linewidth',lineWidth)
% xlabel('r')
% ylabel('diffL')
% legend(repmat("lambda = ",4,1) + num2str(lambda))
% grid minor;
% box off;
% title('Derivtive of loss function')
% set(gca,'fontsize', fontSize + 3);
% saveas(gcf,'fig/diffLoss', 'jpg')

