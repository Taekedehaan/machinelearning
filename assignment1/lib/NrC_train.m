function [r,L, rLength] = NrC_train(x, y, lambda)
    samples = size(x,1);

    n(1) = sum(y==1);
    n(2) = sum(y==2);
    dimensions = size(x,2);
    
    
    itterations = 10000;
    learnRate = 0.5;
    errorLim = 10^(-5);
    
    % Initialize r
    %r = 225*rand(2,64); % zeros
    r = [mean(x(y == 1,:),1); mean(x(y == 2,:),1)];
    
    rLength = nan(2,itterations);
    L = nan(1,itterations);
    
    % gradient decent
    for itteration = 1:itterations
        % reset gradient
        Ldr = zeros(2,64);

        Ldr(1,:) = 2 * (r(1,:)  - mean(x(y == 1,:),1));
        Ldr(2,:) = 2 * (r(2,:)  - mean(x(y == 2,:),1));
        % compute gradient for first term
        %for sample = 1:samples
        %   Ldr(y(sample),:) = Ldr(y(sample),:) + 2/n(y(sample)) * (r(y(sample),:) - x(sample,:)); %% error
        %end  
        
        % compute gradient for second term
       for d = 1:dimensions
           if (r(1,d)-r(2,d)) ~= 0
                Ldr(1,d) = Ldr(1,d) + lambda * (r(1,d)- r(2,d))./abs(r(1,d)-r(2,d));
                Ldr(2,d) = Ldr(2,d) + lambda * (r(2,d)- r(1,d))./abs(r(1,d)-r(2,d));
           end
       end
        
        r = r  - learnRate * Ldr;
        
        % check for nans
        if any(any(isnan(Ldr)))
            warning(['nan in Ldr on itteration:', num2str(itteration)]);
        end
        
        
        rLength(:,itteration) = sum(r(:,:).^2,2);
        L(itteration) = totalCost(r,y,x,n,lambda);
        if L(itteration) == 0
            break;
        end
        if itteration > 1
            if L(itteration) > L(itteration - 1)
                learnRate = learnRate / 2;
            end
            if abs((L(itteration) - L(itteration - 1))/L(itteration-1)) < errorLim
            %    disp('STOP')
                break;
            end
        end
        %disp(learnRate);
    end
end

