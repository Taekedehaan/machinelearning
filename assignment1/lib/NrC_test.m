function [pCorrect,pError] = NrC_test(x, y, r)
    nTot = size(x,1);

    rZero = nan(nTot,1);
    rOne  = nan(nTot,1);
    yDetermined = zeros(nTot,1);
    
    %% classify
    for sample = 1:nTot 
        rZero(sample,1) = sqrt(sum((x(sample,:) - r(1,:)).^2));
        rOne(sample,1) = sqrt(sum((x(sample,:) - r(2,:)).^2));
    end    

    yDetermined(rZero <= rOne) = 1;
    yDetermined(rZero > rOne) = 2;

    correct = sum(yDetermined == y);
    error = sum(yDetermined ~= y);
    
    if correct + error ~= nTot
        warning('Not all samples have been classified');
    end
    
    
    
    pCorrect = correct/nTot;
    pError = error/nTot;
end

