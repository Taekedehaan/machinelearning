function [xTest,yTest, xTrain, yTrain] = split_data(data, pTrain)
    
    % data size
    nData = size(data,1);
    iData = size(data,2) - 1;
    
    % size of training pool
    if pTrain == 1
        nTrain = 1;
    else
        nTrain = round(pTrain * nData);
    end
    
    %create randowm vectors
    iRandom = randperm(nData);
    
    if nTrain == 1
        iTrain1 = iRandom(find(data(iRandom,end) == 1,1));
        iTrain2 = iRandom(find(data(iRandom,end) == 2,1));
        
        iTest = iRandom;
        iTrain = [iTrain1,iTrain2];
        
        for i = 1:length(iTrain)
            iTest(iTest == iTrain(i)) = [];
        end
    else
        iTrain = iRandom(1:nTrain);
        iTest = iRandom((nTrain+1):nData);
    end
    
    % selec tdata
    xTest = data(iTest, 1:iData);
    yTest = data(iTest, iData + 1);
    
    xTrain = data(iTrain, 1:iData);
    yTrain = data(iTrain, iData + 1);
    
    if length(iTest) + length(iTrain) ~= nData
        warning('problem with test and train data size');
    end
end

