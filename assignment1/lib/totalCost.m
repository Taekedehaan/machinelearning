function L = totalCost(r,y,x,n,lambda)
    
    % init distance
    L = 0;
    
    % find distance
    for sample = 1:sum(n)    
        L = L + 1/n(y(sample)) * sum((x(sample,:) - r(y(sample),:)).^2);   
    end
    
    % add second term
    L = L + lambda * sum(abs(r(1,:) - r(2,:)));
end