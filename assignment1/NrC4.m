close all
clear all
clc

%% init
% parmeters
d = 64;
lambdas = [0];


% layout
lineWidth = 3;
fontSize = 20;

% load data 
fileName = 'optdigitsubset.txt';
fileID = fopen(fileName);

% read trext file and convert to double
A = textscan(fileID,repmat('%d' ,1,d)); 
x = double(cell2mat(A));

% define indexes of zero's and once's
nTot = length(x);
n = [554, 571];

% split data in two sets
xZero = x(1:n(1),:);
xOne = x((n(1)  + 1):nTot,:);

% label data
y = [ones(n(1), 1);2*ones(nTot - n(1), 1)];

count = 0;
for lambda = lambdas
    count = count + 1;
    [r,LCurve, rLength] = optimize_NrC(y, x, lambda);

end


figure('Position',[200,200,1000,500]);
semilogy(LCurve,'Linewidth',lineWidth)
grid minor
xlabel('Itteration [-]')
ylabel('Cost [-]')
set(gca,'fontsize', fontSize);
saveas(gcf,'fig/itteration', 'jpg')

figure('Position',[200,200,1000,500]);
semilogy(rLength','Linewidth',lineWidth)
grid minor
xlabel('Itteration [-]')
ylabel('length [-]')
set(gca,'fontsize', fontSize);
saveas(gcf,'fig/rLength', 'jpg')

distZero = sqrt(sum(r(1,:).^2 - mean(xZero).^2));
distZero = sqrt(sum(r(1,:).^2 - mean(xZero).^2));


%% classify
for sample = 1:nTot 
    rZero(sample,1) = sqrt(sum((x(sample,:) - r(1,:)).^2));
    rOne(sample,1) = sqrt(sum((x(sample,:) - r(2,:)).^2));
end    

yDetermined = zeros(sum(n),1);
yDetermined(rZero <= rOne) = 1;
yDetermined(rZero > rOne) = 2;
    
correct = sum(yDetermined == y);
wrong = sum(yDetermined ~= y);

if correct + wrong ~= sum(n)
    warning('Not all samples have been classified');
end

disp(correct/sum(n));

%% plot some dimensions
nStart = 5;
nEnd = nStart + 1;

% plot
figure
plot(x(1:n(1),nStart),x(1:n(1),nEnd), 'o');
hold on
plot(r(1,nStart),r(1,nEnd),'o')
hold on
plot(x((n(1)+1):sum(n),nStart),x((n(1)+1):sum(n),nEnd), '*')
hold on
plot(r(2,nStart),r(2,nEnd),'*')
legend('0','r0','1','r1');


%% weigthts
figure
subplot(1,2,1)
title('Zero vector')
imshow(mat2img(r(1,:)),'InitialMagnification','fit');
subplot(1,2,2)
title('One vector')
imshow(mat2img(r(2,:)),'InitialMagnification','fit');
saveas(gcf,"fig/rVis" + num2str(lambda), 'jpg')

figure
for i = 1:100
    subplot(10,10,i)
    imshow(mat2img(x(i+500,:)),'InitialMagnification','fit');
end
